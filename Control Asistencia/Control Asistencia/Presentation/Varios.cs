﻿using ProcesoControlAsistencia.Data;
using ProcesoControlAsistencia.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Control_Asistencia.Presentation
{
    public partial class Varios : Form
    {
        public Varios()
        {
            InitializeComponent();
        }

        private void Varios_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Owner.Show();
        }

        private void bAceptarAccionAusencia_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("¿Desea actualizar los registros?", "Actualizar Registro", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    ControlrrhhOperations.changeParametro(Constants._DB_PARAMETROS[0], this.tAccion.Text);
                    ControlrrhhOperations.changeParametro(Constants._DB_PARAMETROS[1], this.tAusencia.Text);
                    MessageBox.Show("Registros actualizados con éxito.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Varios_Load(object sender, EventArgs e)
        {
            try
            {
                var tipoaccion = ControlrrhhOperations.getParametro(Constants._DB_PARAMETROS[0]);
                var tipoausencia = ControlrrhhOperations.getParametro(Constants._DB_PARAMETROS[1]);
                var usmigracion = ControlrrhhOperations.getParametro(Constants._DB_PARAMETROS[2]);
                var usaprobar = ControlrrhhOperations.getParametro(Constants._DB_PARAMETROS[3]);
                this.tAccion.Text = tipoaccion != null ? tipoaccion.valor : "";
                this.tAusencia.Text = tipoausencia != null ? tipoausencia.valor : "";
                this.tMigrar.Text = usmigracion != null ? usmigracion.valor : "";
                this.tAprobar.Text = usaprobar != null ? usaprobar.valor : "";
                this.getNombreAccion();
                this.getNombreAusencia();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void bAceptarUsuarios_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("¿Desea actualizar los registros?", "Actualizar Registro", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    ControlrrhhOperations.changeParametro(Constants._DB_PARAMETROS[2], this.tMigrar.Text);
                    ControlrrhhOperations.changeParametro(Constants._DB_PARAMETROS[3], this.tAprobar.Text);
                    MessageBox.Show("Registros actualizados con éxito.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void tMigrar_Leave(object sender, EventArgs e)
        {
            
        }

        private void getNombreAccion()
        {
            try
            {
                var nombre = ExactusOperations.getNombreTipoAccion(tAccion.Text);
                this.lDatoAccion.Text = string.IsNullOrEmpty(nombre) ? "No encontrado" : nombre;
                this.bAceptarAccionAusencia.Enabled = lDatoAccion.Text != "No encontrado" && lDatoAusencia.Text != "No encontrado";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void getNombreAusencia()
        {
            try
            {
                var nombre = ExactusOperations.getNombreTipoAusencia(tAusencia.Text);
                this.lDatoAusencia.Text = string.IsNullOrEmpty(nombre) ? "No encontrado" : nombre;
                this.bAceptarAccionAusencia.Enabled = lDatoAccion.Text != "No encontrado" && lDatoAusencia.Text != "No encontrado";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void tAccion_TextChanged(object sender, EventArgs e)
        {
            this.getNombreAccion();
        }

        private void tAusencia_TextChanged(object sender, EventArgs e)
        {
            this.getNombreAusencia();
        }
    }
}
