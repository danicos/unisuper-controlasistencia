﻿using Control_Asistencia.Domain;
using ProcesoControlAsistencia.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Control_Asistencia.Presentation
{
    public partial class Usuarios : Form
    {
        private int indexGrid = 0;
        private int indexMaxGrid = 0;

        public Usuarios()
        {
            InitializeComponent();
        }

        private void Usuarios_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Owner.Show();
        }

        private void bNuevo_Click(object sender, EventArgs e)
        {
            NuevoUsuario form = new NuevoUsuario();
            form.Show(this);
            this.Hide();
        }

        private void Usuarios_Load(object sender, EventArgs e)
        {
        }

        private void setIndexMaxGrid()
        {
            try
            {
                var count = ControlrrhhOperations.getCountUsuarios();
                if (count > 0)
                {
                    double result = (double)count / (double)ConstantsViews._NUMBER_OF_ITEMS;
                    this.indexMaxGrid = (int)Math.Ceiling(result);
                    this.indexMaxGrid--;
                }
                else
                {
                    this.indexMaxGrid = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CargarUsuarios(int index)
        {
            try
            {
                var users = ControlrrhhOperations.getUsuarios(index * ConstantsViews._NUMBER_OF_ITEMS, ConstantsViews._NUMBER_OF_ITEMS);
                this.dgvUsuarios.DataSource = users;
                if (dgvUsuarios.Columns["Accion"] != null)
                {
                    dgvUsuarios.Columns.Remove("Accion");
                }
                DataGridViewButtonColumn deletecolumn = new DataGridViewButtonColumn();
                deletecolumn.Name = "Accion";
                deletecolumn.Text = "Eliminar";
                deletecolumn.UseColumnTextForButtonValue = true;
                this.dgvUsuarios.Columns.Insert(3, deletecolumn);
                this.lIndexGrid.Text = (users.Rows.Count > 0)
                    ? (index + 1).ToString() + "/" + (this.indexMaxGrid + 1).ToString()
                    : "/";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgvUsuarios_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                this.dgvUsuarios.Rows[e.RowIndex].ReadOnly = true;
            }
            if (e.ColumnIndex == this.dgvUsuarios.Columns["Accion"].Index)
            {
                var value = this.dgvUsuarios.Rows[e.RowIndex].Cells[0].Value;
                this.EliminarRegistro(value.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.indexGrid = 0;
            this.CargarUsuarios(this.indexGrid);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (indexGrid > 0)
            {
                this.indexGrid--;
                this.CargarUsuarios(this.indexGrid);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (indexGrid < indexMaxGrid)
            {
                this.indexGrid++;
                this.CargarUsuarios(this.indexGrid);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.indexGrid = indexMaxGrid;
            this.CargarUsuarios(this.indexGrid);
        }

        private void Usuarios_Shown(object sender, EventArgs e)
        {
        }

        private void Usuarios_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                this.setIndexMaxGrid();
                this.CargarUsuarios(this.indexGrid);
            }
        }

        private void EliminarRegistro(String key)
        {
            try
            {
                if (MessageBox.Show("¿Desea eliminar el registro?", "Eliminar Registro", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    ControlrrhhOperations.removePermisos(key);
                    ControlrrhhOperations.removeUsuario(key);
                    this.setIndexMaxGrid();
                    this.indexGrid = 0;
                    this.CargarUsuarios(this.indexGrid);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgvUsuarios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var id = this.dgvUsuarios.Rows[e.RowIndex].Cells[0].Value.ToString();
            var form = new NuevoUsuario();
            form.idusuario = id;
            form.Show(this);
            this.Hide();
        }

        private void dgvUsuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
