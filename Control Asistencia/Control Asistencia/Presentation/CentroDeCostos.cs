﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using ProcesoControlAsistencia.Data;
using ProcesoControlAsistencia.Domain;
using Control_Asistencia.Domain;

namespace Control_Asistencia.Presentation
{
    public partial class CentroDeCostos : Form
    {
        private int indexGrid = 0;
        private int indexMaxGrid = 0;
        public CentroDeCostos()
        {
            InitializeComponent();
        }

        private void CentroDeCostos_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Owner.Show();
        }

        private void bAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                CentroCosto cc = (CentroCosto) this.cbCentroCosto.SelectedValue;
                ControlrrhhOperations.addCentroCosto(cc);
                this.setIndexMaxGrid();
                this.indexGrid = this.indexMaxGrid;
                this.CargarCentrosCostoProceso(this.indexGrid);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CentroDeCostos_Load(object sender, EventArgs e)
        {
            this.CargarCentrosCosto();
            this.setIndexMaxGrid();
            this.CargarCentrosCostoProceso(this.indexGrid);
        }

        private void cbCentroCosto_SelectedIndexChanged(object sender, EventArgs e)
        {
            bAgregar.Enabled = this.cbCentroCosto.SelectedValue != null;
        }

        private void CargarCentrosCosto()
        {
            try
            {
                var centros = ExactusOperations.getCentrosCosto();
                this.cbCentroCosto.DataSource = centros;
                this.cbCentroCosto.DisplayMember = "nombre";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void CargarCentrosCostoProceso(int index)
        {
            try
            {
                var centros = ControlrrhhOperations.getCentrosCosto(index * ConstantsViews._NUMBER_OF_ITEMS, ConstantsViews._NUMBER_OF_ITEMS);
                this.dgvCentrosCosto.DataSource = centros;
                if (dgvCentrosCosto.Columns["Accion"] != null)
                {
                    dgvCentrosCosto.Columns.Remove("Accion");
                }
                DataGridViewButtonColumn deletecolumn = new DataGridViewButtonColumn();
                deletecolumn.Name = "Accion";
                deletecolumn.Text = "Eliminar";
                deletecolumn.UseColumnTextForButtonValue = true;
                this.dgvCentrosCosto.Columns.Insert(2, deletecolumn);
                this.lIndexGrid.Text = (centros.Rows.Count > 0)
                    ? (index + 1).ToString() + "/" + (this.indexMaxGrid + 1).ToString()
                    : "/";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void setIndexMaxGrid()
        {
            try
            {
                var count = ControlrrhhOperations.getCountCentrosCosto();
                if (count > 0)
                {
                    double result = (double)count / (double)ConstantsViews._NUMBER_OF_ITEMS;
                    this.indexMaxGrid = (int)Math.Ceiling(result);
                    this.indexMaxGrid--;
                    this.lNo.Text = count.ToString();
                }
                else
                {
                    this.indexMaxGrid = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgvCentrosCosto_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                this.dgvCentrosCosto.Rows[e.RowIndex].ReadOnly = true;
            }
            if (e.ColumnIndex == this.dgvCentrosCosto.Columns["Accion"].Index)
            {
                var value = this.dgvCentrosCosto.Rows[e.RowIndex].Cells[0].Value;
                this.EliminarRegistro(value.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.indexGrid = 0;
            this.CargarCentrosCostoProceso(this.indexGrid);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (indexGrid > 0)
            {
                this.indexGrid--;
                this.CargarCentrosCostoProceso(this.indexGrid);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (indexGrid < indexMaxGrid)
            {
                this.indexGrid++;
                this.CargarCentrosCostoProceso(this.indexGrid);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.indexGrid = indexMaxGrid;
            this.CargarCentrosCostoProceso(this.indexGrid);
        }

        private void EliminarRegistro(String key)
        {
            try
            {
                if (MessageBox.Show("¿Desea eliminar el registro?", "Eliminar Registro", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    ControlrrhhOperations.removeCentroCosto(key);
                    this.setIndexMaxGrid();
                    this.indexGrid = 0;
                    this.CargarCentrosCostoProceso(this.indexGrid);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }
}
