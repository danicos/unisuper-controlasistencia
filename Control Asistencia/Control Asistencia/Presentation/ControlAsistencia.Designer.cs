﻿namespace Control_Asistencia.Presentation
{
    partial class ControlAsistencia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlAsistencia));
            this.l1 = new System.Windows.Forms.Label();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.lCentroCosto = new System.Windows.Forms.Label();
            this.cbCentrosCosto = new System.Windows.Forms.ComboBox();
            this.bEjecutar = new System.Windows.Forms.Button();
            this.lEstado = new System.Windows.Forms.Label();
            this.lDatoEstado = new System.Windows.Forms.Label();
            this.chCentrosCosto = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listbLog = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pbproceso = new System.Windows.Forms.ProgressBar();
            this.bDownload = new System.Windows.Forms.Button();
            this.sfdReporte = new System.Windows.Forms.SaveFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // l1
            // 
            this.l1.AutoSize = true;
            this.l1.Font = new System.Drawing.Font("Verdana", 10F);
            this.l1.Location = new System.Drawing.Point(69, 98);
            this.l1.Name = "l1";
            this.l1.Size = new System.Drawing.Size(48, 17);
            this.l1.TabIndex = 0;
            this.l1.Text = "Fecha";
            // 
            // dtpFecha
            // 
            this.dtpFecha.CalendarFont = new System.Drawing.Font("Verdana", 10F);
            this.dtpFecha.Font = new System.Drawing.Font("Verdana", 10F);
            this.dtpFecha.Location = new System.Drawing.Point(129, 92);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(284, 24);
            this.dtpFecha.TabIndex = 2;
            this.dtpFecha.ValueChanged += new System.EventHandler(this.dtpFecha_ValueChanged);
            // 
            // lCentroCosto
            // 
            this.lCentroCosto.AutoSize = true;
            this.lCentroCosto.Font = new System.Drawing.Font("Verdana", 10F);
            this.lCentroCosto.Location = new System.Drawing.Point(14, 139);
            this.lCentroCosto.Name = "lCentroCosto";
            this.lCentroCosto.Size = new System.Drawing.Size(103, 17);
            this.lCentroCosto.TabIndex = 3;
            this.lCentroCosto.Text = "Centro Costo";
            // 
            // cbCentrosCosto
            // 
            this.cbCentrosCosto.Enabled = false;
            this.cbCentrosCosto.Font = new System.Drawing.Font("Verdana", 10F);
            this.cbCentrosCosto.FormattingEnabled = true;
            this.cbCentrosCosto.Location = new System.Drawing.Point(129, 136);
            this.cbCentrosCosto.Name = "cbCentrosCosto";
            this.cbCentrosCosto.Size = new System.Drawing.Size(250, 24);
            this.cbCentrosCosto.TabIndex = 4;
            this.cbCentrosCosto.SelectedIndexChanged += new System.EventHandler(this.cbCentrosCosto_SelectedIndexChanged);
            // 
            // bEjecutar
            // 
            this.bEjecutar.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.bEjecutar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bEjecutar.Font = new System.Drawing.Font("Verdana", 10F);
            this.bEjecutar.Image = ((System.Drawing.Image)(resources.GetObject("bEjecutar.Image")));
            this.bEjecutar.Location = new System.Drawing.Point(221, 183);
            this.bEjecutar.Name = "bEjecutar";
            this.bEjecutar.Size = new System.Drawing.Size(94, 32);
            this.bEjecutar.TabIndex = 5;
            this.bEjecutar.Text = "Ejecutar";
            this.bEjecutar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bEjecutar.UseVisualStyleBackColor = true;
            this.bEjecutar.Click += new System.EventHandler(this.bEjecutar_Click);
            // 
            // lEstado
            // 
            this.lEstado.AutoSize = true;
            this.lEstado.Font = new System.Drawing.Font("Verdana", 10F);
            this.lEstado.Location = new System.Drawing.Point(43, 19);
            this.lEstado.Name = "lEstado";
            this.lEstado.Size = new System.Drawing.Size(57, 17);
            this.lEstado.TabIndex = 6;
            this.lEstado.Text = "Estado";
            // 
            // lDatoEstado
            // 
            this.lDatoEstado.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold);
            this.lDatoEstado.Location = new System.Drawing.Point(3, 48);
            this.lDatoEstado.Name = "lDatoEstado";
            this.lDatoEstado.Size = new System.Drawing.Size(134, 20);
            this.lDatoEstado.TabIndex = 7;
            this.lDatoEstado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chCentrosCosto
            // 
            this.chCentrosCosto.AutoSize = true;
            this.chCentrosCosto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chCentrosCosto.Location = new System.Drawing.Point(396, 142);
            this.chCentrosCosto.Name = "chCentrosCosto";
            this.chCentrosCosto.Size = new System.Drawing.Size(12, 11);
            this.chCentrosCosto.TabIndex = 8;
            this.chCentrosCosto.UseVisualStyleBackColor = true;
            this.chCentrosCosto.CheckedChanged += new System.EventHandler(this.chCentrosCosto_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 10F);
            this.label1.Location = new System.Drawing.Point(9, 216);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Resultado";
            // 
            // listbLog
            // 
            this.listbLog.Font = new System.Drawing.Font("Verdana", 10F);
            this.listbLog.FormattingEnabled = true;
            this.listbLog.ItemHeight = 16;
            this.listbLog.Location = new System.Drawing.Point(12, 236);
            this.listbLog.Name = "listbLog";
            this.listbLog.Size = new System.Drawing.Size(560, 116);
            this.listbLog.TabIndex = 11;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(204)))));
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(584, 50);
            this.panel1.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 14.25F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(15, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 23);
            this.label2.TabIndex = 0;
            this.label2.Text = "Ejecutar Proceso";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lDatoEstado);
            this.panel2.Controls.Add(this.lEstado);
            this.panel2.Location = new System.Drawing.Point(430, 72);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(142, 88);
            this.panel2.TabIndex = 13;
            // 
            // pbproceso
            // 
            this.pbproceso.Location = new System.Drawing.Point(0, 49);
            this.pbproceso.Name = "pbproceso";
            this.pbproceso.Size = new System.Drawing.Size(584, 10);
            this.pbproceso.TabIndex = 14;
            // 
            // bDownload
            // 
            this.bDownload.Enabled = false;
            this.bDownload.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.bDownload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bDownload.Font = new System.Drawing.Font("Verdana", 10F);
            this.bDownload.Image = ((System.Drawing.Image)(resources.GetObject("bDownload.Image")));
            this.bDownload.Location = new System.Drawing.Point(430, 175);
            this.bDownload.Name = "bDownload";
            this.bDownload.Size = new System.Drawing.Size(142, 47);
            this.bDownload.TabIndex = 15;
            this.bDownload.Text = "Descargar Reporte";
            this.bDownload.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bDownload.UseVisualStyleBackColor = true;
            this.bDownload.Click += new System.EventHandler(this.bDownload_Click);
            // 
            // sfdReporte
            // 
            this.sfdReporte.DefaultExt = "csv";
            this.sfdReporte.Filter = "CSV files (*.csv)|";
            // 
            // ControlAsistencia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(584, 362);
            this.Controls.Add(this.bDownload);
            this.Controls.Add(this.bEjecutar);
            this.Controls.Add(this.pbproceso);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.listbLog);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chCentrosCosto);
            this.Controls.Add(this.cbCentrosCosto);
            this.Controls.Add(this.lCentroCosto);
            this.Controls.Add(this.dtpFecha);
            this.Controls.Add(this.l1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ControlAsistencia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Control de Asistencia";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ControlAsistencia_FormClosed);
            this.Load += new System.EventHandler(this.ControlAsistencia_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label l1;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label lCentroCosto;
        private System.Windows.Forms.ComboBox cbCentrosCosto;
        private System.Windows.Forms.Button bEjecutar;
        private System.Windows.Forms.Label lEstado;
        private System.Windows.Forms.Label lDatoEstado;
        private System.Windows.Forms.CheckBox chCentrosCosto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listbLog;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ProgressBar pbproceso;
        private System.Windows.Forms.Button bDownload;
        private System.Windows.Forms.SaveFileDialog sfdReporte;
    }
}