﻿namespace Control_Asistencia.Presentation
{
    partial class Varios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Varios));
            this.tcVarios = new System.Windows.Forms.TabControl();
            this.tpAccionAusencia = new System.Windows.Forms.TabPage();
            this.bAceptarAccionAusencia = new System.Windows.Forms.Button();
            this.lDatoAusencia = new System.Windows.Forms.Label();
            this.tAusencia = new System.Windows.Forms.TextBox();
            this.lausencia = new System.Windows.Forms.Label();
            this.lDatoAccion = new System.Windows.Forms.Label();
            this.lNombreAccion = new System.Windows.Forms.Label();
            this.tAccion = new System.Windows.Forms.TextBox();
            this.laccion = new System.Windows.Forms.Label();
            this.tpUsuarios = new System.Windows.Forms.TabPage();
            this.bAceptarUsuarios = new System.Windows.Forms.Button();
            this.tAprobar = new System.Windows.Forms.TextBox();
            this.lAprobar = new System.Windows.Forms.Label();
            this.tMigrar = new System.Windows.Forms.TextBox();
            this.lMigrar = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.tcVarios.SuspendLayout();
            this.tpAccionAusencia.SuspendLayout();
            this.tpUsuarios.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcVarios
            // 
            this.tcVarios.Controls.Add(this.tpAccionAusencia);
            this.tcVarios.Controls.Add(this.tpUsuarios);
            this.tcVarios.Font = new System.Drawing.Font("Verdana", 10F);
            this.tcVarios.Location = new System.Drawing.Point(12, 76);
            this.tcVarios.Name = "tcVarios";
            this.tcVarios.SelectedIndex = 0;
            this.tcVarios.Size = new System.Drawing.Size(560, 274);
            this.tcVarios.TabIndex = 0;
            // 
            // tpAccionAusencia
            // 
            this.tpAccionAusencia.Controls.Add(this.bAceptarAccionAusencia);
            this.tpAccionAusencia.Controls.Add(this.lDatoAusencia);
            this.tpAccionAusencia.Controls.Add(this.tAusencia);
            this.tpAccionAusencia.Controls.Add(this.lausencia);
            this.tpAccionAusencia.Controls.Add(this.lDatoAccion);
            this.tpAccionAusencia.Controls.Add(this.lNombreAccion);
            this.tpAccionAusencia.Controls.Add(this.tAccion);
            this.tpAccionAusencia.Controls.Add(this.laccion);
            this.tpAccionAusencia.Location = new System.Drawing.Point(4, 25);
            this.tpAccionAusencia.Name = "tpAccionAusencia";
            this.tpAccionAusencia.Padding = new System.Windows.Forms.Padding(3);
            this.tpAccionAusencia.Size = new System.Drawing.Size(552, 245);
            this.tpAccionAusencia.TabIndex = 0;
            this.tpAccionAusencia.Text = "Tipo accion / ausencia";
            this.tpAccionAusencia.UseVisualStyleBackColor = true;
            // 
            // bAceptarAccionAusencia
            // 
            this.bAceptarAccionAusencia.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.bAceptarAccionAusencia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bAceptarAccionAusencia.Image = ((System.Drawing.Image)(resources.GetObject("bAceptarAccionAusencia.Image")));
            this.bAceptarAccionAusencia.Location = new System.Drawing.Point(441, 198);
            this.bAceptarAccionAusencia.Name = "bAceptarAccionAusencia";
            this.bAceptarAccionAusencia.Size = new System.Drawing.Size(94, 32);
            this.bAceptarAccionAusencia.TabIndex = 8;
            this.bAceptarAccionAusencia.Text = "Aceptar";
            this.bAceptarAccionAusencia.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bAceptarAccionAusencia.UseVisualStyleBackColor = true;
            this.bAceptarAccionAusencia.Click += new System.EventHandler(this.bAceptarAccionAusencia_Click);
            // 
            // lDatoAusencia
            // 
            this.lDatoAusencia.Location = new System.Drawing.Point(284, 132);
            this.lDatoAusencia.Name = "lDatoAusencia";
            this.lDatoAusencia.Size = new System.Drawing.Size(251, 42);
            this.lDatoAusencia.TabIndex = 7;
            this.lDatoAusencia.Text = "--";
            this.lDatoAusencia.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tAusencia
            // 
            this.tAusencia.Location = new System.Drawing.Point(130, 132);
            this.tAusencia.Name = "tAusencia";
            this.tAusencia.Size = new System.Drawing.Size(132, 24);
            this.tAusencia.TabIndex = 5;
            this.tAusencia.TextChanged += new System.EventHandler(this.tAusencia_TextChanged);
            // 
            // lausencia
            // 
            this.lausencia.AutoSize = true;
            this.lausencia.Location = new System.Drawing.Point(15, 135);
            this.lausencia.Name = "lausencia";
            this.lausencia.Size = new System.Drawing.Size(105, 17);
            this.lausencia.TabIndex = 4;
            this.lausencia.Text = "Tipo Ausencia";
            // 
            // lDatoAccion
            // 
            this.lDatoAccion.Location = new System.Drawing.Point(284, 71);
            this.lDatoAccion.Name = "lDatoAccion";
            this.lDatoAccion.Size = new System.Drawing.Size(251, 42);
            this.lDatoAccion.TabIndex = 3;
            this.lDatoAccion.Text = "--";
            this.lDatoAccion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lNombreAccion
            // 
            this.lNombreAccion.AutoSize = true;
            this.lNombreAccion.Location = new System.Drawing.Point(379, 29);
            this.lNombreAccion.Name = "lNombreAccion";
            this.lNombreAccion.Size = new System.Drawing.Size(63, 17);
            this.lNombreAccion.TabIndex = 2;
            this.lNombreAccion.Text = "Nombre";
            // 
            // tAccion
            // 
            this.tAccion.Location = new System.Drawing.Point(130, 71);
            this.tAccion.Name = "tAccion";
            this.tAccion.Size = new System.Drawing.Size(132, 24);
            this.tAccion.TabIndex = 1;
            this.tAccion.TextChanged += new System.EventHandler(this.tAccion_TextChanged);
            // 
            // laccion
            // 
            this.laccion.AutoSize = true;
            this.laccion.Location = new System.Drawing.Point(32, 74);
            this.laccion.Name = "laccion";
            this.laccion.Size = new System.Drawing.Size(88, 17);
            this.laccion.TabIndex = 0;
            this.laccion.Text = "Tipo Acción";
            // 
            // tpUsuarios
            // 
            this.tpUsuarios.Controls.Add(this.bAceptarUsuarios);
            this.tpUsuarios.Controls.Add(this.tAprobar);
            this.tpUsuarios.Controls.Add(this.lAprobar);
            this.tpUsuarios.Controls.Add(this.tMigrar);
            this.tpUsuarios.Controls.Add(this.lMigrar);
            this.tpUsuarios.Location = new System.Drawing.Point(4, 25);
            this.tpUsuarios.Name = "tpUsuarios";
            this.tpUsuarios.Padding = new System.Windows.Forms.Padding(3);
            this.tpUsuarios.Size = new System.Drawing.Size(552, 245);
            this.tpUsuarios.TabIndex = 1;
            this.tpUsuarios.Text = "Usuarios";
            this.tpUsuarios.UseVisualStyleBackColor = true;
            // 
            // bAceptarUsuarios
            // 
            this.bAceptarUsuarios.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.bAceptarUsuarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bAceptarUsuarios.Image = ((System.Drawing.Image)(resources.GetObject("bAceptarUsuarios.Image")));
            this.bAceptarUsuarios.Location = new System.Drawing.Point(441, 198);
            this.bAceptarUsuarios.Name = "bAceptarUsuarios";
            this.bAceptarUsuarios.Size = new System.Drawing.Size(94, 32);
            this.bAceptarUsuarios.TabIndex = 17;
            this.bAceptarUsuarios.Text = "Aceptar";
            this.bAceptarUsuarios.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bAceptarUsuarios.UseVisualStyleBackColor = true;
            this.bAceptarUsuarios.Click += new System.EventHandler(this.bAceptarUsuarios_Click);
            // 
            // tAprobar
            // 
            this.tAprobar.Location = new System.Drawing.Point(225, 142);
            this.tAprobar.Name = "tAprobar";
            this.tAprobar.Size = new System.Drawing.Size(203, 24);
            this.tAprobar.TabIndex = 14;
            // 
            // lAprobar
            // 
            this.lAprobar.AutoSize = true;
            this.lAprobar.Location = new System.Drawing.Point(90, 145);
            this.lAprobar.Name = "lAprobar";
            this.lAprobar.Size = new System.Drawing.Size(123, 17);
            this.lAprobar.TabIndex = 13;
            this.lAprobar.Text = "Usuario Aprobar";
            // 
            // tMigrar
            // 
            this.tMigrar.Location = new System.Drawing.Point(225, 76);
            this.tMigrar.Name = "tMigrar";
            this.tMigrar.Size = new System.Drawing.Size(203, 24);
            this.tMigrar.TabIndex = 10;
            this.tMigrar.Leave += new System.EventHandler(this.tMigrar_Leave);
            // 
            // lMigrar
            // 
            this.lMigrar.AutoSize = true;
            this.lMigrar.Location = new System.Drawing.Point(100, 79);
            this.lMigrar.Name = "lMigrar";
            this.lMigrar.Size = new System.Drawing.Size(109, 17);
            this.lMigrar.TabIndex = 9;
            this.lMigrar.Text = "Usuario Migrar";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(204)))));
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(584, 50);
            this.panel1.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 14.25F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(15, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(218, 23);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mantenimiento Varios";
            // 
            // Varios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(584, 362);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tcVarios);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Varios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Control de Asistencia";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Varios_FormClosed);
            this.Load += new System.EventHandler(this.Varios_Load);
            this.tcVarios.ResumeLayout(false);
            this.tpAccionAusencia.ResumeLayout(false);
            this.tpAccionAusencia.PerformLayout();
            this.tpUsuarios.ResumeLayout(false);
            this.tpUsuarios.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcVarios;
        private System.Windows.Forms.TabPage tpAccionAusencia;
        private System.Windows.Forms.Button bAceptarAccionAusencia;
        private System.Windows.Forms.Label lDatoAusencia;
        private System.Windows.Forms.TextBox tAusencia;
        private System.Windows.Forms.Label lausencia;
        private System.Windows.Forms.Label lDatoAccion;
        private System.Windows.Forms.Label lNombreAccion;
        private System.Windows.Forms.TextBox tAccion;
        private System.Windows.Forms.Label laccion;
        private System.Windows.Forms.TabPage tpUsuarios;
        private System.Windows.Forms.Button bAceptarUsuarios;
        private System.Windows.Forms.TextBox tAprobar;
        private System.Windows.Forms.Label lAprobar;
        private System.Windows.Forms.TextBox tMigrar;
        private System.Windows.Forms.Label lMigrar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
    }
}