﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProcesoControlAsistencia.Data;
using ProcesoControlAsistencia.Domain;
using Control_Asistencia.Domain;

namespace Control_Asistencia.Presentation
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void bIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                var usuario = DoLogin();
                Menu menu = new Menu();
                menu.usuario = usuario;
                menu.Show(this);
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Login_VisibleChanged(object sender, EventArgs e)
        {
            this.tUsuario.Clear();
            this.tPass.Clear();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            try
            {
                FileManager fl = new FileManager();
                fl.readExactusConnectionConfig();
                fl.readSoftlandcaConnectionConfig();
                fl.readComtrolRRHHConnectionConfig();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private Usuario DoLogin()
        {
            if (String.IsNullOrEmpty(tUsuario.Text.Trim())) throw new Exception(String.Format(ConstantsViews._EMPTY_FIELD, "Nombre"));
            if (String.IsNullOrEmpty(tPass.Text.Trim())) throw new Exception(String.Format(ConstantsViews._EMPTY_FIELD, "Contraseña"));
            var usuario = ControlrrhhOperations.validateUsuario(tUsuario.Text, tPass.Text);
            if (usuario == null) throw new Exception(ConstantsViews._LOGIN_INCORRECTO);
            usuario.permisos = ControlrrhhOperations.getPermisos(usuario.usuario);
            return usuario;
        }

        private void bocultar_Click(object sender, EventArgs e)
        {
            bocultar.Visible = false;
            bver.Visible = true;
            tPass.PasswordChar = char.Parse("*");
            tPass.Focus();
        }

        private void bver_Click(object sender, EventArgs e)
        {
            bocultar.Visible = true;
            bver.Visible = false;
            tPass.PasswordChar = char.Parse("\0");
            tPass.Focus();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
