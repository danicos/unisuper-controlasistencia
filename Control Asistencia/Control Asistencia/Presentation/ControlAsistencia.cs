﻿using Control_Asistencia.Domain;
using ProcesoControlAsistencia.Data;
using ProcesoControlAsistencia.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Control_Asistencia.Presentation
{
    public partial class ControlAsistencia : Form
    {
        StringBuilder csv;

        public ControlAsistencia()
        {
            InitializeComponent();
        }

        private void ControlAsistencia_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Owner.Show();
        }

        private void ControlAsistencia_Load(object sender, EventArgs e)
        {

        }

        private void bEjecutar_Click(object sender, EventArgs e)
        {
            try
            {
                bEjecutar.Visible = false;
                bDownload.Enabled = false;
                pbproceso.Value = 1;
                listbLog.Items.Clear();
                var currentThread = new System.Threading.Thread(doProcess);
                currentThread.Start();
            }
            catch (Exception ex)
            {
                bEjecutar.Visible = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void doProcess()
        {
            //Calculo de la fecha de extraccion de Marcas de Reloj
            DateTime date = this.dtpFecha.Value;
            TimeSpan ts = new TimeSpan(0, 0, 0);
            date = date.Date + ts;
            CentroCosto selectedCentro = null;
            Invoke(new Action(() =>
            {
                selectedCentro = chCentrosCosto.Checked ? (CentroCosto)cbCentrosCosto.SelectedItem : null;
            }));
            updateProgressBar(5);
            this.csv = new StringBuilder();
            addReportHeader();
            Proceso.ejecutarProceso(date, selectedCentro, updateProgressBar, addLog, addReport);
            endProcess();
        }
    
        private void endProcess()
        {
            if (this == null) return;
            Invoke(new Action(() => {
                bEjecutar.Visible = true;
                bDownload.Enabled = true;
            }));
        }

        private void cargarCentrosCosto()
        {
            var centros = ControlrrhhOperations.getCentrosCosto();
            this.cbCentrosCosto.DataSource = centros;
            this.cbCentrosCosto.DisplayMember = "nombre";
        }

        private void chCentrosCosto_CheckedChanged(object sender, EventArgs e)
        {
            if (chCentrosCosto.Checked)
            {
                this.cargarCentrosCosto();
                cbCentrosCosto.Enabled = true;
            }
            else
            {
                cbCentrosCosto.DataSource = null;
                cbCentrosCosto.Enabled = false;
                bEjecutar.Enabled = true;
                lDatoEstado.Text = "";
            }
        }

        private void addLog(String texto)
        {
            if (this == null) return;
            Invoke(new Action(() =>
            {
                listbLog.Items.Add(texto);
            }));
        }

        private void cbCentrosCosto_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbCentrosCosto.SelectedIndex != -1)
            {
                var logproc = ControlrrhhOperations.existsLogProceso(dtpFecha.Value, (CentroCosto) cbCentrosCosto.SelectedValue);
                lDatoEstado.Text = logproc ? "Ejecutado" : "No ejecutado";
                bEjecutar.Enabled = !logproc;
            }
        }

        private void dtpFecha_ValueChanged(object sender, EventArgs e)
        {
            if (chCentrosCosto.Checked)
            {
                var logproc = ControlrrhhOperations.existsLogProceso(dtpFecha.Value, (CentroCosto)cbCentrosCosto.SelectedValue);
                lDatoEstado.Text = logproc ? "Ejecutado" : "No ejecutado";
                bEjecutar.Enabled = !logproc;
            }
        }

        private void updateProgressBar(double n)
        {
            if (this == null) return;
            Invoke(new Action(() => pbproceso.Value = (int)n));
        }

        private void addReportHeader()
        {
            var newLine = string.Format("{0},{1},{2},{3},{4},{5}",
                "Fecha",
                "Centro Costo",
                "Nombre Centro Costo",
                "Empleado",
                "Nombre Empleado",
                "Aplica Descuento"
                );
            csv.AppendLine(newLine); 
        }

        private void addReport(Empleado empleado, DateTime fecha, Boolean descuento)
        {
            if (this == null) return;
            Invoke(new Action(() =>
            {
                var newLine = string.Format("{0},{1},{2},{3},{4},{5}",
                fecha.ToString("yyyy-MM-dd"),
                "<" + empleado.centro_costo + ">",
                empleado.centro_costo_nombre,
                empleado.empleado,
                empleado.nombre,
                descuento ? "Si" : "No"
                );
                csv.AppendLine(newLine);  
            }));
        }

        private void bDownload_Click(object sender, EventArgs e)
        {
            if (sfdReporte.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(sfdReporte.FileName, csv.ToString());
            } 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var acc = ExactusOperations.getAccionesEmpleado(new Empleado { empleado = "39857" }, new DateTime(2018, 8, 14));
            var emp = ControlrrhhOperations.getEmpleadosNoMarcaje();
            var hola = "hola";
        }

    }
}
