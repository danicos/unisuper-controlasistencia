﻿namespace Control_Asistencia.Presentation
{
    partial class CentroDeCostos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CentroDeCostos));
            this.dgvCentrosCosto = new System.Windows.Forms.DataGridView();
            this.lCentroCosto = new System.Windows.Forms.Label();
            this.cbCentroCosto = new System.Windows.Forms.ComboBox();
            this.bAgregar = new System.Windows.Forms.Button();
            this.lNoRegistros = new System.Windows.Forms.Label();
            this.lNo = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.lIndexGrid = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCentrosCosto)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvCentrosCosto
            // 
            this.dgvCentrosCosto.AllowUserToAddRows = false;
            this.dgvCentrosCosto.AllowUserToDeleteRows = false;
            this.dgvCentrosCosto.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCentrosCosto.Location = new System.Drawing.Point(12, 112);
            this.dgvCentrosCosto.MultiSelect = false;
            this.dgvCentrosCosto.Name = "dgvCentrosCosto";
            this.dgvCentrosCosto.Size = new System.Drawing.Size(560, 213);
            this.dgvCentrosCosto.TabIndex = 0;
            this.dgvCentrosCosto.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCentrosCosto_CellClick);
            // 
            // lCentroCosto
            // 
            this.lCentroCosto.AutoSize = true;
            this.lCentroCosto.Font = new System.Drawing.Font("Verdana", 10F);
            this.lCentroCosto.Location = new System.Drawing.Point(51, 74);
            this.lCentroCosto.Name = "lCentroCosto";
            this.lCentroCosto.Size = new System.Drawing.Size(122, 17);
            this.lCentroCosto.TabIndex = 1;
            this.lCentroCosto.Text = "Centro de costo";
            // 
            // cbCentroCosto
            // 
            this.cbCentroCosto.Font = new System.Drawing.Font("Verdana", 10F);
            this.cbCentroCosto.FormattingEnabled = true;
            this.cbCentroCosto.Location = new System.Drawing.Point(192, 74);
            this.cbCentroCosto.Name = "cbCentroCosto";
            this.cbCentroCosto.Size = new System.Drawing.Size(232, 24);
            this.cbCentroCosto.TabIndex = 2;
            this.cbCentroCosto.SelectedIndexChanged += new System.EventHandler(this.cbCentroCosto_SelectedIndexChanged);
            // 
            // bAgregar
            // 
            this.bAgregar.Enabled = false;
            this.bAgregar.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.bAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bAgregar.Font = new System.Drawing.Font("Verdana", 10F);
            this.bAgregar.Image = ((System.Drawing.Image)(resources.GetObject("bAgregar.Image")));
            this.bAgregar.Location = new System.Drawing.Point(436, 69);
            this.bAgregar.Name = "bAgregar";
            this.bAgregar.Size = new System.Drawing.Size(94, 32);
            this.bAgregar.TabIndex = 3;
            this.bAgregar.Text = "Agregar";
            this.bAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bAgregar.UseVisualStyleBackColor = true;
            this.bAgregar.Click += new System.EventHandler(this.bAgregar_Click);
            // 
            // lNoRegistros
            // 
            this.lNoRegistros.AutoSize = true;
            this.lNoRegistros.Font = new System.Drawing.Font("Verdana", 10F);
            this.lNoRegistros.Location = new System.Drawing.Point(9, 335);
            this.lNoRegistros.Name = "lNoRegistros";
            this.lNoRegistros.Size = new System.Drawing.Size(106, 17);
            this.lNoRegistros.TabIndex = 4;
            this.lNoRegistros.Text = "No. registros:";
            // 
            // lNo
            // 
            this.lNo.AutoSize = true;
            this.lNo.Font = new System.Drawing.Font("Verdana", 10F);
            this.lNo.Location = new System.Drawing.Point(121, 335);
            this.lNo.Name = "lNo";
            this.lNo.Size = new System.Drawing.Size(22, 17);
            this.lNo.TabIndex = 5;
            this.lNo.Text = "--";
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(436, 331);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(22, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "<";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(398, 331);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(32, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "<<";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button4
            // 
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(512, 331);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(22, 23);
            this.button4.TabIndex = 8;
            this.button4.Text = ">";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(540, 331);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(32, 23);
            this.button3.TabIndex = 9;
            this.button3.Text = ">>";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // lIndexGrid
            // 
            this.lIndexGrid.Font = new System.Drawing.Font("Verdana", 10F);
            this.lIndexGrid.Location = new System.Drawing.Point(464, 331);
            this.lIndexGrid.Name = "lIndexGrid";
            this.lIndexGrid.Size = new System.Drawing.Size(42, 22);
            this.lIndexGrid.TabIndex = 10;
            this.lIndexGrid.Text = "/";
            this.lIndexGrid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(204)))));
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(584, 50);
            this.panel1.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 14.25F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(15, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(322, 23);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mantenimiento Centros de Costo";
            // 
            // CentroDeCostos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(584, 362);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lIndexGrid);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lNo);
            this.Controls.Add(this.lNoRegistros);
            this.Controls.Add(this.bAgregar);
            this.Controls.Add(this.cbCentroCosto);
            this.Controls.Add(this.lCentroCosto);
            this.Controls.Add(this.dgvCentrosCosto);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CentroDeCostos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Control de Asistencia";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CentroDeCostos_FormClosed);
            this.Load += new System.EventHandler(this.CentroDeCostos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCentrosCosto)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvCentrosCosto;
        private System.Windows.Forms.Label lCentroCosto;
        private System.Windows.Forms.ComboBox cbCentroCosto;
        private System.Windows.Forms.Button bAgregar;
        private System.Windows.Forms.Label lNoRegistros;
        private System.Windows.Forms.Label lNo;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label lIndexGrid;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
    }
}