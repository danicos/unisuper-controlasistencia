﻿namespace Control_Asistencia.Presentation
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.l1 = new System.Windows.Forms.Label();
            this.tUsuario = new System.Windows.Forms.TextBox();
            this.tPass = new System.Windows.Forms.TextBox();
            this.bIngresar = new System.Windows.Forms.Button();
            this.lUsuario = new System.Windows.Forms.Label();
            this.lPass = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bver = new System.Windows.Forms.Button();
            this.bocultar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // l1
            // 
            this.l1.AutoSize = true;
            this.l1.Font = new System.Drawing.Font("Verdana", 14.25F);
            this.l1.ForeColor = System.Drawing.Color.White;
            this.l1.Location = new System.Drawing.Point(7, 15);
            this.l1.Name = "l1";
            this.l1.Size = new System.Drawing.Size(213, 23);
            this.l1.TabIndex = 0;
            this.l1.Text = "Control de Asistencia";
            // 
            // tUsuario
            // 
            this.tUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tUsuario.Font = new System.Drawing.Font("Verdana", 10F);
            this.tUsuario.Location = new System.Drawing.Point(129, 155);
            this.tUsuario.Name = "tUsuario";
            this.tUsuario.Size = new System.Drawing.Size(177, 24);
            this.tUsuario.TabIndex = 1;
            // 
            // tPass
            // 
            this.tPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tPass.Font = new System.Drawing.Font("Verdana", 10F);
            this.tPass.Location = new System.Drawing.Point(129, 206);
            this.tPass.Name = "tPass";
            this.tPass.PasswordChar = '*';
            this.tPass.Size = new System.Drawing.Size(177, 24);
            this.tPass.TabIndex = 2;
            // 
            // bIngresar
            // 
            this.bIngresar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.bIngresar.Cursor = System.Windows.Forms.Cursors.Default;
            this.bIngresar.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.bIngresar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bIngresar.Font = new System.Drawing.Font("Verdana", 10F);
            this.bIngresar.Image = ((System.Drawing.Image)(resources.GetObject("bIngresar.Image")));
            this.bIngresar.Location = new System.Drawing.Point(125, 260);
            this.bIngresar.Name = "bIngresar";
            this.bIngresar.Size = new System.Drawing.Size(129, 31);
            this.bIngresar.TabIndex = 3;
            this.bIngresar.Text = "Ingresar";
            this.bIngresar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bIngresar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bIngresar.UseVisualStyleBackColor = false;
            this.bIngresar.Click += new System.EventHandler(this.bIngresar_Click);
            // 
            // lUsuario
            // 
            this.lUsuario.AutoSize = true;
            this.lUsuario.Font = new System.Drawing.Font("Verdana", 10F);
            this.lUsuario.Location = new System.Drawing.Point(53, 157);
            this.lUsuario.Name = "lUsuario";
            this.lUsuario.Size = new System.Drawing.Size(61, 17);
            this.lUsuario.TabIndex = 4;
            this.lUsuario.Text = "Usuario";
            // 
            // lPass
            // 
            this.lPass.AutoSize = true;
            this.lPass.Font = new System.Drawing.Font("Verdana", 10F);
            this.lPass.Location = new System.Drawing.Point(25, 211);
            this.lPass.Name = "lPass";
            this.lPass.Size = new System.Drawing.Size(89, 17);
            this.lPass.TabIndex = 5;
            this.lPass.Text = "Contraseña";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(204)))));
            this.panel1.Controls.Add(this.l1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(384, 50);
            this.panel1.TabIndex = 6;
            // 
            // bver
            // 
            this.bver.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.bver.FlatAppearance.BorderSize = 0;
            this.bver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bver.Font = new System.Drawing.Font("Verdana", 10F);
            this.bver.Image = ((System.Drawing.Image)(resources.GetObject("bver.Image")));
            this.bver.Location = new System.Drawing.Point(312, 207);
            this.bver.Name = "bver";
            this.bver.Size = new System.Drawing.Size(25, 24);
            this.bver.TabIndex = 24;
            this.bver.UseVisualStyleBackColor = true;
            this.bver.Click += new System.EventHandler(this.bver_Click);
            // 
            // bocultar
            // 
            this.bocultar.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.bocultar.FlatAppearance.BorderSize = 0;
            this.bocultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bocultar.Font = new System.Drawing.Font("Verdana", 10F);
            this.bocultar.Image = ((System.Drawing.Image)(resources.GetObject("bocultar.Image")));
            this.bocultar.Location = new System.Drawing.Point(312, 207);
            this.bocultar.Name = "bocultar";
            this.bocultar.Size = new System.Drawing.Size(25, 24);
            this.bocultar.TabIndex = 25;
            this.bocultar.UseVisualStyleBackColor = true;
            this.bocultar.Visible = false;
            this.bocultar.Click += new System.EventHandler(this.bocultar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::Control_Asistencia.Properties.Resources.UNISUPER_LOGOTIPO;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(97, 52);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(194, 82);
            this.pictureBox1.TabIndex = 27;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(384, 312);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lPass);
            this.Controls.Add(this.lUsuario);
            this.Controls.Add(this.bIngresar);
            this.Controls.Add(this.tPass);
            this.Controls.Add(this.tUsuario);
            this.Controls.Add(this.bver);
            this.Controls.Add(this.bocultar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Control de Asistencia";
            this.Load += new System.EventHandler(this.Login_Load);
            this.VisibleChanged += new System.EventHandler(this.Login_VisibleChanged);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label l1;
        private System.Windows.Forms.TextBox tUsuario;
        private System.Windows.Forms.TextBox tPass;
        private System.Windows.Forms.Button bIngresar;
        private System.Windows.Forms.Label lUsuario;
        private System.Windows.Forms.Label lPass;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button bver;
        private System.Windows.Forms.Button bocultar;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}