﻿namespace Control_Asistencia.Presentation
{
    partial class NuevoUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NuevoUsuario));
            this.label1 = new System.Windows.Forms.Label();
            this.lNombre = new System.Windows.Forms.Label();
            this.tNombre = new System.Windows.Forms.TextBox();
            this.tUsuario = new System.Windows.Forms.TextBox();
            this.lUsuario = new System.Windows.Forms.Label();
            this.tPass = new System.Windows.Forms.TextBox();
            this.lPass = new System.Windows.Forms.Label();
            this.lEstado = new System.Windows.Forms.Label();
            this.cbEstado = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.bAceptar = new System.Windows.Forms.Button();
            this.chblPermisos = new System.Windows.Forms.CheckedListBox();
            this.bGuardar = new System.Windows.Forms.Button();
            this.bCancelar = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.bver = new System.Windows.Forms.Button();
            this.bocultar = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 10F);
            this.label1.Location = new System.Drawing.Point(22, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Datos del usuario";
            // 
            // lNombre
            // 
            this.lNombre.AutoSize = true;
            this.lNombre.Font = new System.Drawing.Font("Verdana", 10F);
            this.lNombre.Location = new System.Drawing.Point(307, 113);
            this.lNombre.Name = "lNombre";
            this.lNombre.Size = new System.Drawing.Size(63, 17);
            this.lNombre.TabIndex = 1;
            this.lNombre.Text = "Nombre";
            // 
            // tNombre
            // 
            this.tNombre.Font = new System.Drawing.Font("Verdana", 10F);
            this.tNombre.Location = new System.Drawing.Point(376, 110);
            this.tNombre.Name = "tNombre";
            this.tNombre.Size = new System.Drawing.Size(165, 24);
            this.tNombre.TabIndex = 4;
            // 
            // tUsuario
            // 
            this.tUsuario.Font = new System.Drawing.Font("Verdana", 10F);
            this.tUsuario.Location = new System.Drawing.Point(125, 110);
            this.tUsuario.Name = "tUsuario";
            this.tUsuario.Size = new System.Drawing.Size(165, 24);
            this.tUsuario.TabIndex = 2;
            // 
            // lUsuario
            // 
            this.lUsuario.AutoSize = true;
            this.lUsuario.Font = new System.Drawing.Font("Verdana", 10F);
            this.lUsuario.Location = new System.Drawing.Point(58, 113);
            this.lUsuario.Name = "lUsuario";
            this.lUsuario.Size = new System.Drawing.Size(61, 17);
            this.lUsuario.TabIndex = 3;
            this.lUsuario.Text = "Usuario";
            // 
            // tPass
            // 
            this.tPass.Font = new System.Drawing.Font("Verdana", 10F);
            this.tPass.Location = new System.Drawing.Point(125, 150);
            this.tPass.Name = "tPass";
            this.tPass.PasswordChar = '*';
            this.tPass.Size = new System.Drawing.Size(143, 24);
            this.tPass.TabIndex = 6;
            // 
            // lPass
            // 
            this.lPass.AutoSize = true;
            this.lPass.Font = new System.Drawing.Font("Verdana", 10F);
            this.lPass.Location = new System.Drawing.Point(30, 153);
            this.lPass.Name = "lPass";
            this.lPass.Size = new System.Drawing.Size(89, 17);
            this.lPass.TabIndex = 5;
            this.lPass.Text = "Contraseña";
            this.lPass.Click += new System.EventHandler(this.label4_Click);
            // 
            // lEstado
            // 
            this.lEstado.AutoSize = true;
            this.lEstado.Font = new System.Drawing.Font("Verdana", 10F);
            this.lEstado.Location = new System.Drawing.Point(313, 153);
            this.lEstado.Name = "lEstado";
            this.lEstado.Size = new System.Drawing.Size(57, 17);
            this.lEstado.TabIndex = 7;
            this.lEstado.Text = "Estado";
            // 
            // cbEstado
            // 
            this.cbEstado.Font = new System.Drawing.Font("Verdana", 10F);
            this.cbEstado.FormattingEnabled = true;
            this.cbEstado.Location = new System.Drawing.Point(376, 150);
            this.cbEstado.Name = "cbEstado";
            this.cbEstado.Size = new System.Drawing.Size(165, 24);
            this.cbEstado.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 10F);
            this.label6.Location = new System.Drawing.Point(45, 203);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 17);
            this.label6.TabIndex = 9;
            this.label6.Text = "Permisos";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // bAceptar
            // 
            this.bAceptar.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.bAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bAceptar.Font = new System.Drawing.Font("Verdana", 10F);
            this.bAceptar.Image = ((System.Drawing.Image)(resources.GetObject("bAceptar.Image")));
            this.bAceptar.Location = new System.Drawing.Point(340, 354);
            this.bAceptar.Name = "bAceptar";
            this.bAceptar.Size = new System.Drawing.Size(94, 32);
            this.bAceptar.TabIndex = 11;
            this.bAceptar.Text = "Aceptar";
            this.bAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bAceptar.UseVisualStyleBackColor = true;
            this.bAceptar.Click += new System.EventHandler(this.bAceptar_Click);
            // 
            // chblPermisos
            // 
            this.chblPermisos.Font = new System.Drawing.Font("Verdana", 10F);
            this.chblPermisos.FormattingEnabled = true;
            this.chblPermisos.Location = new System.Drawing.Point(48, 235);
            this.chblPermisos.Name = "chblPermisos";
            this.chblPermisos.Size = new System.Drawing.Size(477, 99);
            this.chblPermisos.TabIndex = 19;
            this.chblPermisos.SelectedIndexChanged += new System.EventHandler(this.chblPermisos_SelectedIndexChanged);
            // 
            // bGuardar
            // 
            this.bGuardar.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.bGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bGuardar.Font = new System.Drawing.Font("Verdana", 10F);
            this.bGuardar.Image = ((System.Drawing.Image)(resources.GetObject("bGuardar.Image")));
            this.bGuardar.Location = new System.Drawing.Point(340, 354);
            this.bGuardar.Name = "bGuardar";
            this.bGuardar.Size = new System.Drawing.Size(94, 32);
            this.bGuardar.TabIndex = 20;
            this.bGuardar.Text = "Guardar";
            this.bGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bGuardar.UseVisualStyleBackColor = true;
            this.bGuardar.Click += new System.EventHandler(this.bGuardar_Click);
            // 
            // bCancelar
            // 
            this.bCancelar.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.bCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bCancelar.Font = new System.Drawing.Font("Verdana", 10F);
            this.bCancelar.Image = ((System.Drawing.Image)(resources.GetObject("bCancelar.Image")));
            this.bCancelar.Location = new System.Drawing.Point(451, 354);
            this.bCancelar.Name = "bCancelar";
            this.bCancelar.Size = new System.Drawing.Size(94, 32);
            this.bCancelar.TabIndex = 21;
            this.bCancelar.Text = "Cancelar";
            this.bCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bCancelar.UseVisualStyleBackColor = true;
            this.bCancelar.Click += new System.EventHandler(this.bCancelar_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(204)))));
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(584, 50);
            this.panel1.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 14.25F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(15, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(241, 23);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mantenimiento Usuarios";
            // 
            // bver
            // 
            this.bver.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.bver.FlatAppearance.BorderSize = 0;
            this.bver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bver.Font = new System.Drawing.Font("Verdana", 10F);
            this.bver.Image = ((System.Drawing.Image)(resources.GetObject("bver.Image")));
            this.bver.Location = new System.Drawing.Point(274, 149);
            this.bver.Name = "bver";
            this.bver.Size = new System.Drawing.Size(25, 24);
            this.bver.TabIndex = 23;
            this.bver.UseVisualStyleBackColor = true;
            this.bver.Click += new System.EventHandler(this.bver_Click);
            // 
            // bocultar
            // 
            this.bocultar.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.bocultar.FlatAppearance.BorderSize = 0;
            this.bocultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bocultar.Font = new System.Drawing.Font("Verdana", 10F);
            this.bocultar.Image = ((System.Drawing.Image)(resources.GetObject("bocultar.Image")));
            this.bocultar.Location = new System.Drawing.Point(274, 149);
            this.bocultar.Name = "bocultar";
            this.bocultar.Size = new System.Drawing.Size(25, 24);
            this.bocultar.TabIndex = 24;
            this.bocultar.UseVisualStyleBackColor = true;
            this.bocultar.Visible = false;
            this.bocultar.Click += new System.EventHandler(this.bocultar_Click);
            // 
            // NuevoUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(584, 398);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bCancelar);
            this.Controls.Add(this.chblPermisos);
            this.Controls.Add(this.bAceptar);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbEstado);
            this.Controls.Add(this.lEstado);
            this.Controls.Add(this.tPass);
            this.Controls.Add(this.lPass);
            this.Controls.Add(this.tUsuario);
            this.Controls.Add(this.lUsuario);
            this.Controls.Add(this.tNombre);
            this.Controls.Add(this.lNombre);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bver);
            this.Controls.Add(this.bocultar);
            this.Controls.Add(this.bGuardar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NuevoUsuario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Control de Asistencia";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.NuevoUsuario_FormClosed);
            this.Load += new System.EventHandler(this.NuevoUsuario_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lNombre;
        private System.Windows.Forms.TextBox tNombre;
        private System.Windows.Forms.TextBox tUsuario;
        private System.Windows.Forms.Label lUsuario;
        private System.Windows.Forms.TextBox tPass;
        private System.Windows.Forms.Label lPass;
        private System.Windows.Forms.Label lEstado;
        private System.Windows.Forms.ComboBox cbEstado;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button bAceptar;
        private System.Windows.Forms.CheckedListBox chblPermisos;
        private System.Windows.Forms.Button bGuardar;
        private System.Windows.Forms.Button bCancelar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bver;
        private System.Windows.Forms.Button bocultar;
    }
}