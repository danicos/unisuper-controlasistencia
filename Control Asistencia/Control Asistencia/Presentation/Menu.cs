﻿using ProcesoControlAsistencia.Data;
using ProcesoControlAsistencia.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Control_Asistencia.Presentation
{
    public partial class Menu : Form
    {
        public Usuario usuario;

        public Menu()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Owner.Show();
            this.Close();
        }

        private void Menu_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Owner.Show();
        }

        private void bEjecutarProceso_Click(object sender, EventArgs e)
        {
            ControlAsistencia form = new ControlAsistencia();
            form.Show(this);
            this.Hide();
        }

        private void bCentroCostos_Click(object sender, EventArgs e)
        {
            CentroDeCostos form = new CentroDeCostos();
            form.Show(this);
            this.Hide();
        }

        private void bVarios_Click(object sender, EventArgs e)
        {
            Varios form = new Varios();
            form.Show(this);
            this.Hide();
        }

        private void bUsuarios_Click(object sender, EventArgs e)
        {
            Usuarios form = new Usuarios();
            form.Show(this);
            this.Hide();
        }

        private void Menu_Load(object sender, EventArgs e)
        {
            bEjecutarProceso.Visible = buscarPermiso(Constants._DB_PER[0]);
            bCentroCostos.Visible = buscarPermiso(Constants._DB_PER[1]);
            bVarios.Visible = buscarPermiso(Constants._DB_PER[2]);
            bUsuarios.Visible = buscarPermiso(Constants._DB_PER[3]);
        }

        private Boolean buscarPermiso(String permiso)
        {
            foreach (var per in usuario.permisos)
            {
                if (per.cod == permiso)
                {
                    return true;
                }
            }
            return false;
        }

    }
}
