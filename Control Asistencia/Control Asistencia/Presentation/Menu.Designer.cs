﻿namespace Control_Asistencia.Presentation
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.bEjecutarProceso = new System.Windows.Forms.Button();
            this.bCentroCostos = new System.Windows.Forms.Button();
            this.bVarios = new System.Windows.Forms.Button();
            this.bUsuarios = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.l1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bEjecutarProceso
            // 
            this.bEjecutarProceso.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.bEjecutarProceso.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bEjecutarProceso.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.bEjecutarProceso.Image = ((System.Drawing.Image)(resources.GetObject("bEjecutarProceso.Image")));
            this.bEjecutarProceso.Location = new System.Drawing.Point(61, 68);
            this.bEjecutarProceso.Name = "bEjecutarProceso";
            this.bEjecutarProceso.Size = new System.Drawing.Size(130, 110);
            this.bEjecutarProceso.TabIndex = 0;
            this.bEjecutarProceso.Text = "Ejecutar Proceso";
            this.bEjecutarProceso.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bEjecutarProceso.UseVisualStyleBackColor = true;
            this.bEjecutarProceso.Click += new System.EventHandler(this.bEjecutarProceso_Click);
            // 
            // bCentroCostos
            // 
            this.bCentroCostos.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.bCentroCostos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bCentroCostos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.bCentroCostos.Image = ((System.Drawing.Image)(resources.GetObject("bCentroCostos.Image")));
            this.bCentroCostos.Location = new System.Drawing.Point(197, 68);
            this.bCentroCostos.Name = "bCentroCostos";
            this.bCentroCostos.Size = new System.Drawing.Size(130, 110);
            this.bCentroCostos.TabIndex = 1;
            this.bCentroCostos.Text = "Mantenimiento Centro de Costos";
            this.bCentroCostos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bCentroCostos.UseVisualStyleBackColor = true;
            this.bCentroCostos.Click += new System.EventHandler(this.bCentroCostos_Click);
            // 
            // bVarios
            // 
            this.bVarios.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.bVarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bVarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.bVarios.Image = ((System.Drawing.Image)(resources.GetObject("bVarios.Image")));
            this.bVarios.Location = new System.Drawing.Point(61, 184);
            this.bVarios.Name = "bVarios";
            this.bVarios.Size = new System.Drawing.Size(130, 110);
            this.bVarios.TabIndex = 2;
            this.bVarios.Text = "Mantenimiento Varios";
            this.bVarios.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bVarios.UseVisualStyleBackColor = true;
            this.bVarios.Click += new System.EventHandler(this.bVarios_Click);
            // 
            // bUsuarios
            // 
            this.bUsuarios.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.bUsuarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bUsuarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.bUsuarios.Image = ((System.Drawing.Image)(resources.GetObject("bUsuarios.Image")));
            this.bUsuarios.Location = new System.Drawing.Point(197, 184);
            this.bUsuarios.Name = "bUsuarios";
            this.bUsuarios.Size = new System.Drawing.Size(130, 110);
            this.bUsuarios.TabIndex = 3;
            this.bUsuarios.Text = "Mantenimiento Usuarios";
            this.bUsuarios.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bUsuarios.UseVisualStyleBackColor = true;
            this.bUsuarios.Click += new System.EventHandler(this.bUsuarios_Click);
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(309, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(74, 50);
            this.button1.TabIndex = 4;
            this.button1.Text = "Salir";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(204)))));
            this.panel1.Controls.Add(this.l1);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(384, 50);
            this.panel1.TabIndex = 7;
            // 
            // l1
            // 
            this.l1.AutoSize = true;
            this.l1.Font = new System.Drawing.Font("Verdana", 14.25F);
            this.l1.ForeColor = System.Drawing.Color.White;
            this.l1.Location = new System.Drawing.Point(15, 15);
            this.l1.Name = "l1";
            this.l1.Size = new System.Drawing.Size(61, 23);
            this.l1.TabIndex = 0;
            this.l1.Text = "Menú";
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(384, 312);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bUsuarios);
            this.Controls.Add(this.bVarios);
            this.Controls.Add(this.bCentroCostos);
            this.Controls.Add(this.bEjecutarProceso);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Control de Asistencia";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Menu_FormClosed);
            this.Load += new System.EventHandler(this.Menu_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bEjecutarProceso;
        private System.Windows.Forms.Button bCentroCostos;
        private System.Windows.Forms.Button bVarios;
        private System.Windows.Forms.Button bUsuarios;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label l1;
    }
}