﻿using Control_Asistencia.Domain;
using ProcesoControlAsistencia.Data;
using ProcesoControlAsistencia.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Control_Asistencia.Presentation
{
    public partial class NuevoUsuario : Form
    {
        public string idusuario;

        public NuevoUsuario()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void NuevoUsuario_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Owner.Show();
        }

        private void NuevoUsuario_Load(object sender, EventArgs e)
        {
            try
            {
                this.cargarEstados();
                this.cargarPermisos();
                Boolean enable = this.idusuario == null;
                this.tUsuario.Enabled = enable;
                this.bGuardar.Visible = !enable;
                this.bAceptar.Visible = enable;
                if (!enable) cargarUsuario();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void bAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                addUsuario();
                MessageBox.Show(ConstantsViews._AGREGADO_EXITOSO);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void addUsuario()
        {
            if (String.IsNullOrEmpty(tNombre.Text.Trim())) throw new Exception(String.Format(ConstantsViews._EMPTY_FIELD,"Nombre"));
            if (String.IsNullOrEmpty(tUsuario.Text.Trim())) throw new Exception(String.Format(ConstantsViews._EMPTY_FIELD,"Usuario"));
            if (String.IsNullOrEmpty(tPass.Text.Trim())) throw new Exception(String.Format(ConstantsViews._EMPTY_FIELD, "Contraseña"));
            if (String.IsNullOrEmpty(((String)cbEstado.SelectedValue))) throw new Exception(String.Format(ConstantsViews._EMPTY_FIELD, "Estado"));

            var usuario = new Usuario();
            usuario.nombre = tNombre.Text;
            usuario.usuario = tUsuario.Text;
            usuario.contrasenia = tPass.Text;
            usuario.estado = cbEstado.SelectedValue.ToString();

            ControlrrhhOperations.addUsuario(usuario);

            var permisos = chblPermisos.CheckedItems;
            foreach (var permiso in permisos)
            {
                ControlrrhhOperations.addPermiso(usuario, (Permiso) permiso);
            }
        }

        private void cargarEstados()
        {
            cbEstado.DataSource = Constants._USER_STATES.ToArray().ToList();
            cbEstado.DisplayMember = "clave";
            cbEstado.ValueMember = "valor";
        }

        private void cargarUsuario()
        {
            try
            {
                var usuario = ControlrrhhOperations.getUsuario(this.idusuario);
                if (usuario == null)
                {
                    MessageBox.Show(ConstantsViews._REGISTRO_NO_EXISTE);
                    this.Close();
                    return;
                }
                tUsuario.Text = usuario.usuario;
                tNombre.Text = usuario.nombre;
                cbEstado.SelectedValue = usuario.estado;
                var permisos = ControlrrhhOperations.getPermisos(this.idusuario);
                foreach (var permiso in permisos)
                {
                    for (int i = 0; i < chblPermisos.Items.Count; i++)
                    {
                        if (permiso.cod == ((Permiso)chblPermisos.Items[i]).cod)
                        {
                            chblPermisos.SetItemChecked(i, true);
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cargarPermisos()
        {
            ((ListBox)chblPermisos).DataSource = ControlrrhhOperations.getPermisos();
            ((ListBox)chblPermisos).DisplayMember = "descripcion";
        }

        private void bCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                updateUsuario();
                MessageBox.Show(ConstantsViews._ACTUALIZADO_EXITOSO);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void updateUsuario()
        {
            if (String.IsNullOrEmpty(tNombre.Text.Trim())) throw new Exception(String.Format(ConstantsViews._EMPTY_FIELD, "Nombre"));
            if (String.IsNullOrEmpty(((String)cbEstado.SelectedValue))) throw new Exception(String.Format(ConstantsViews._EMPTY_FIELD, "Estado"));

            var usuario = new Usuario();
            usuario.nombre = tNombre.Text;
            usuario.usuario = this.idusuario;
            usuario.contrasenia = String.IsNullOrEmpty(tPass.Text.Trim()) ? null : tPass.Text;
            usuario.estado = cbEstado.SelectedValue.ToString();

            ControlrrhhOperations.updateUsuario(usuario);
            ControlrrhhOperations.removePermisos(usuario.usuario);
            var permisos = chblPermisos.CheckedItems;
            foreach (var permiso in permisos)
            {
                ControlrrhhOperations.addPermiso(usuario, (Permiso)permiso);
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {
            var s =UniCrypto.encryptInnova("eltirodelguerrero");
            Console.WriteLine(s);
        }

        private void chblPermisos_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void bocultar_Click(object sender, EventArgs e)
        {
            bocultar.Visible = false;
            bver.Visible = true;
            tPass.PasswordChar = char.Parse("*");
        }

        private void bver_Click(object sender, EventArgs e)
        {
            bocultar.Visible = true;
            bver.Visible = false;
            tPass.PasswordChar = char.Parse("\0");
        }
    }
}
