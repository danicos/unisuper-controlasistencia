﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control_Asistencia.Domain
{
    public static class ConstantsViews
    {
        public const int _NUMBER_OF_ITEMS = 10;
        public const String _LOG_NOCENTROCOSTO = "El proceso ya ha sido ejecutado anteriormente para el centro de costo <{0}>.";
        public const String _LOG_PROCESOEXITO = "Proceso ejecutado con éxito para el centro de costo <{0}>.";
        public const String _LOG_NOEJECUTADO = "El proceso no pudo ser ejecutado en ningun centro de costo.";
        public const String _EMPTY_FIELD = "El campo <{0}> es obligatorio.";
        public const String _AGREGADO_EXITOSO = "Registro agregado con éxito.";
        public const String _ACTUALIZADO_EXITOSO = "Registro actualizado con éxito.";
        public const String _REGISTRO_NO_EXISTE = "El registro no existe.";
        public const String _LOGIN_INCORRECTO = "Usuario o Contraseña incorrecta.";
    }
}
