﻿using ProcesoControlAsistencia.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control_Asistencia_Ejecutable
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                FileManager fl = new FileManager();
                fl.readExactusConnectionConfig();
                fl.readSoftlandcaConnectionConfig();
                fl.readComtrolRRHHConnectionConfig();
                Proceso.ejecutarProceso(calculateDate(), null, null, writeLog);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static DateTime calculateDate()
        {
            var now = DateTime.Now;
            //Calculo de la fecha de extraccion de Marcas de Reloj
            TimeSpan ts = new TimeSpan(0, 0, 0);
            now = now.Date + ts;
            return now.AddDays(-1);
        }

        private static void writeLog(String txt)
        {
            Console.WriteLine(txt);
        }
    }
}
