/*
SQLyog Ultimate v10.11 
MySQL - 5.7.18-log : Database - dbcontrolrrhh
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dbcontrolrrhh` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `dbcontrolrrhh`;

/*Table structure for table `centrocosto` */

DROP TABLE IF EXISTS `centrocosto`;

CREATE TABLE `centrocosto` (
  `rowindex` float NOT NULL AUTO_INCREMENT,
  `centrocosto` varchar(50) NOT NULL,
  `nombre` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`centrocosto`),
  KEY `rowindex` (`rowindex`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

/*Data for the table `centrocosto` */

/*Table structure for table `empleadosreloj` */

DROP TABLE IF EXISTS `empleadosreloj`;

CREATE TABLE `empleadosreloj` (
  `reloj_idregistro` float NOT NULL,
  `reloj_codempleado` float DEFAULT NULL,
  `reloj_fechaentrada` datetime DEFAULT NULL,
  PRIMARY KEY (`reloj_idregistro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `empleadosreloj` */

/*Table structure for table `empleadossoftland` */

DROP TABLE IF EXISTS `empleadossoftland`;

CREATE TABLE `empleadossoftland` (
  `soft_codempleado` varchar(15) NOT NULL,
  `soft_nombre` varchar(300) DEFAULT NULL,
  `soft_activo` char(1) DEFAULT NULL,
  `soft_centrocosto` varchar(50) DEFAULT NULL,
  `soft_nombrecentrocosto` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`soft_codempleado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `empleadossoftland` */

/*Table structure for table `logcontrolentradas` */

DROP TABLE IF EXISTS `logcontrolentradas`;

CREATE TABLE `logcontrolentradas` (
  `log_id` float NOT NULL AUTO_INCREMENT,
  `log_codempleado` varchar(15) DEFAULT NULL,
  `log_nombreempleado` varchar(300) DEFAULT NULL,
  `log_centrocosto` varchar(50) DEFAULT NULL,
  `log_fechaentrada` date DEFAULT NULL,
  `log_fecharegistro` date DEFAULT NULL,
  `log_descuento` char(1) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3015 DEFAULT CHARSET=utf8;

/*Data for the table `logcontrolentradas` */

/*Table structure for table `logproceso` */

DROP TABLE IF EXISTS `logproceso`;

CREATE TABLE `logproceso` (
  `logp_fechaentrada` date NOT NULL,
  `logp_centrocosto` varchar(15) NOT NULL,
  `logp_fecharegistro` date DEFAULT NULL,
  PRIMARY KEY (`logp_fechaentrada`,`logp_centrocosto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `logproceso` */

/*Table structure for table `parametroconfig` */

DROP TABLE IF EXISTS `parametroconfig`;

CREATE TABLE `parametroconfig` (
  `clave` varchar(5) NOT NULL,
  `valor` varchar(150) DEFAULT NULL,
  `descripcion` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `parametroconfig` */

insert  into `parametroconfig`(`clave`,`valor`,`descripcion`) values ('NOFAL','1','Numero de Faltas'),('TIPAC','','Tipo de Accion'),('TIPAU','','Tipo de Ausencia'),('USAPR','','Usuario Aprobar'),('USMIG','','Usuario Migracion');

/*Table structure for table `permiso` */

DROP TABLE IF EXISTS `permiso`;

CREATE TABLE `permiso` (
  `cod` varchar(5) NOT NULL,
  `descripcion` varchar(150) NOT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `permiso` */

insert  into `permiso`(`cod`,`descripcion`) values ('EJPRO','Ejecutar proceso'),('MANCC','Mantenimiento centro de costo'),('MANUS','Mantenimiento usuarios'),('MANVA','Mantenimiento varios');

/*Table structure for table `usuario` */

DROP TABLE IF EXISTS `usuario`;

CREATE TABLE `usuario` (
  `rowindex` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(15) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `contrasenia` varchar(65) NOT NULL,
  `estado` char(1) DEFAULT NULL,
  PRIMARY KEY (`usuario`),
  KEY `rowindex` (`rowindex`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `usuario` */

insert  into `usuario`(`rowindex`,`usuario`,`nombre`,`contrasenia`,`estado`) values (13,'admin','Administrador','j75EwMmK0X9TYylQVPq+TF9oW5g=','A');

/*Table structure for table `usuario_permiso` */

DROP TABLE IF EXISTS `usuario_permiso`;

CREATE TABLE `usuario_permiso` (
  `usuario` varchar(15) NOT NULL,
  `permiso` varchar(5) NOT NULL,
  PRIMARY KEY (`usuario`,`permiso`),
  KEY `usuario_permiso_permiso` (`permiso`),
  CONSTRAINT `usuario_permiso_permiso` FOREIGN KEY (`permiso`) REFERENCES `permiso` (`cod`),
  CONSTRAINT `usuario_permiso_usuario` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `usuario_permiso` */

insert  into `usuario_permiso`(`usuario`,`permiso`) values ('admin','EJPRO'),('admin','MANCC'),('admin','MANUS'),('admin','MANVA');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
