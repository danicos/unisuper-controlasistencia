﻿using MySql.Data.MySqlClient;
using ProcesoControlAsistencia.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcesoControlAsistencia.Data
{
    public static class ControlrrhhOperations
    {
        public static DataConnection dc;

        #region ** centrocosto

        public static void addCentroCosto(CentroCosto centrocosto)
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_INSERT_CENTROCOSTO, centrocosto.codigo, centrocosto.nombre);
                    executeQuery(db, query);
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable getCentrosCosto(int index, int count)
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_SELECT_CENTROCOSTO, index.ToString(), count.ToString());
                    var dt = executeQuerySelect(db, query);
                    dt.Columns["nombre"].ColumnName = "Nombre";
                    dt.Columns["centrocosto"].ColumnName = "Centro de Costo";
                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<CentroCosto> getCentrosCosto()
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_SELECT_ALL_CENTROCOSTO);
                    var dt = executeQuerySelect(db, query);
                    dt.Columns["nombre"].ColumnName = "nombre";
                    dt.Columns["centrocosto"].ColumnName = "codigo";
                    return Utils.ConvertDataTable<CentroCosto>(dt);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int getCountCentrosCosto()
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = SQLQuery._Q_COUNT_CENTROCOSTO;
                    return executeQueryCount(db, query);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void removeCentroCosto(String key)
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_DELETE_CENTROCOSTO, key);
                    executeQuery(db, query);
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ** usuarios

        public static void addUsuario(Usuario usuario)
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_INSERT_USUARIO, usuario.usuario, usuario.nombre, UniCrypto.encryptKey(usuario.contrasenia), usuario.estado);
                    executeQuery(db, query);
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable getUsuarios(int index, int count)
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_SELECT_USUARIO, index.ToString(), count.ToString());
                    var dt = executeQuerySelect(db, query);
                    dt.Columns["usuario"].ColumnName = "Usuario";
                    dt.Columns["nombre"].ColumnName = "Nombre";
                    dt.Columns["estado"].ColumnName = "Estado";
                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Usuario getUsuario(String id)
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_SELECT_USUARIOBYID, id);
                    var dt = executeQuerySelect(db, query);
                    var usuarios = Utils.ConvertDataTable<Usuario>(dt);
                    return usuarios.Count() > 0 ? usuarios[0] : null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Usuario> getUsuarios()
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_SELECT_ALL_CENTROCOSTO);
                    var dt = executeQuerySelect(db, query);
                    dt.Columns["nombre"].ColumnName = "nombre";
                    dt.Columns["centrocosto"].ColumnName = "codigo";
                    return Utils.ConvertDataTable<Usuario>(dt);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int getCountUsuarios()
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = SQLQuery._Q_COUNT_USUARIOS;
                    return executeQueryCount(db, query);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void removeUsuario(String key)
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_DELETE_USUARIO, key);
                    executeQuery(db, query);
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void updateUsuario(Usuario usuario)
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = usuario.contrasenia == null
                        ? String.Format(SQLQuery._Q_UPDATE_USUARIO, usuario.nombre, usuario.estado, usuario.usuario)
                        : String.Format(SQLQuery._Q_UPDATE_USUARIOPASS, usuario.nombre, UniCrypto.encryptKey(usuario.contrasenia), usuario.estado, usuario.usuario);
                    executeQuery(db, query);
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Usuario validateUsuario(String usuario, String contrasenia)
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_SELECT_USUARIOPASS, usuario, UniCrypto.encryptKey(contrasenia));
                    var dt = executeQuerySelect(db, query);
                    var usuarios = Utils.ConvertDataTable<Usuario>(dt);
                    return usuarios.Count() > 0 ? usuarios[0] : null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ** empleadosreloj

        public static void removeAllEmpleadoReloj()
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_DELETE_ALL_EMPLEADORELOJ);
                    executeQuery(db, query);
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void addEmpleadoReloj(List<Marca> marcas, Action<double> Estado = null)
        {
            try
            {
                using (var db = createConnection())
                {
                    double aum = 35.00 / marcas.Count();
                    double val = 35;
                    foreach (var marca in marcas)
                    {
                        val += aum;
                        if (Estado != null) Estado(val);
                        String query = String.Format(SQLQuery._Q_INSERT_EMPLEADORELOJ, marca.registro, marca.empleado, marca.fecha.Year, marca.fecha.Month, marca.fecha.Day);
                        executeQuery(db, query);
                    }
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ** empleadossoftland
        
        public static void removeAllEmpleadoSoft()
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_DELETE_ALL_EMPLEADOSOFT);
                    executeQuery(db, query);
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void addEmpleadoSoft(List<Empleado> empleados, Action<double> Estado)
        {
            try
            {
                using (var db = createConnection())
                {
                    double aum = 25.00 / empleados.Count();
                    double val = 10;
                    foreach (var empleado in empleados)
                    {
                        val += aum;
                        if (Estado != null) Estado(val);
                        String query = String.Format(SQLQuery._Q_INSERT_EMPLEADOSOFT, empleado.empleado, empleado.nombre, empleado.activo, empleado.centro_costo, empleado.centro_costo_nombre);
                        executeQuery(db, query);
                    }
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        #endregion

        #region ** proceso

        public static List<Empleado> getEmpleadosNoMarcaje()
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_SELECT_EMPLEADOS_NO_MARCA);
                    var dt = executeQuerySelect(db, query);
                    dt.Columns["soft_codempleado"].ColumnName = "empleado";
                    dt.Columns["soft_nombre"].ColumnName = "nombre";
                    dt.Columns["soft_activo"].ColumnName = "activo";
                    dt.Columns["soft_centrocosto"].ColumnName = "centro_costo";
                    dt.Columns["soft_nombrecentrocosto"].ColumnName = "centro_costo_nombre";
                    return Utils.ConvertDataTable<Empleado>(dt);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Falta> getFaltasByRangeDate(Empleado empleado, DateTime startDate, DateTime endDate)
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_SELECT_LOG_NO_DESCUENTO, empleado.empleado, startDate.Year, startDate.Month, startDate.Day, endDate.Year, endDate.Month, endDate.Day);
                    var dt = executeQuerySelect(db, query);
                    return Utils.ConvertDataTable<Falta>(dt);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ** parametros

        public static void changeParametro(String key, String value)
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_UPDATE_PARAMETRO, value, key);
                    executeQuery(db, query);
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Parametro getParametro(String key)
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_SELECT_PARAMETRO, key);
                    var dt = executeQuerySelect(db, query);
                    var parametros = Utils.ConvertDataTable<Parametro>(dt);
                    return parametros.Count() > 0 ? parametros[0] : null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ** logentradas

        public static void addLogEntrada(Empleado empleado, DateTime fecha, Boolean descuento)
        {
            try
            {
                using (var db = createConnection())
                {
                    var today = DateTime.Now;
                    String query = String.Format(SQLQuery._Q_INSERT_LOGENTRADA, 
                        empleado.empleado, 
                        empleado.nombre,
                        empleado.centro_costo,
                        fecha.Year, fecha.Month, fecha.Day,
                        today.Year, today.Month, today.Day,
                        descuento ? "S" : "N"
                        );
                    executeQuery(db, query);
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void addLogProceso(DateTime fecha, CentroCosto centro)
        {
            try
            {
                using (var db = createConnection())
                {
                    var today = DateTime.Now;
                    String query = String.Format(SQLQuery._Q_INSERT_LOGPROCESO,
                        fecha.Year, fecha.Month, fecha.Day,
                        centro.codigo,
                        today.Year, today.Month, today.Day);
                    executeQuery(db, query);
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Boolean existsLogProceso(DateTime fecha, CentroCosto centro)
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_SELECT_LOGPROCESO, fecha.Year, fecha.Month, fecha.Day, centro.codigo);
                    var dt = executeQuerySelect(db, query);
                    return dt.Rows.Count > 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ** permisos

        public static List<Permiso> getPermisos()
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_SELECT_PERMISOS);
                    var dt = executeQuerySelect(db, query);
                    return Utils.ConvertDataTable<Permiso>(dt);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Permiso> getPermisos(String usuario)
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_SELECT_PERMISOSUS, usuario);
                    var dt = executeQuerySelect(db, query);
                    return Utils.ConvertDataTable<Permiso>(dt);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void addPermiso(Usuario usuario, Permiso permiso)
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_INSERT_PERMISO, usuario.usuario, permiso.cod);
                    executeQuery(db, query);
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void removePermisos(String usuario)
        {
            try
            {
                using (var db = createConnection())
                {
                    String query = String.Format(SQLQuery._Q_REMOVE_PERMISOS, usuario);
                    executeQuery(db, query);
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        private static void executeQuery(MySqlConnection db, String query)
        {
            try
            {
                MySqlCommand cmd = new MySqlCommand(query, db);
                MySqlDataReader dr;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception(SQLException.getMessageByNumberError(ex.Number));
            }
            catch (Exception ex)
            {
                throw new Exception(ErrorMessage._ERR_NO_EXECUTE_QUERY, ex);
            }
        }

        private static DataTable executeQuerySelect(MySqlConnection db, String query)
        {
            try
            {
                MySqlDataAdapter da = new MySqlDataAdapter();
                da.SelectCommand = new MySqlCommand(query, db);
                DataTable table = new DataTable();
                da.Fill(table);
                db.Close();
                return table;
            }
            catch (MySqlException ex)
            {
                throw new Exception(SQLException.getMessageByNumberError(ex.Number));
            }
            catch (Exception ex)
            {
                throw new Exception(ErrorMessage._ERR_NO_EXECUTE_QUERY, ex);
            }
        }

        private static int executeQueryCount(MySqlConnection db, String query)
        {
            try
            {
                MySqlCommand cmd = new MySqlCommand(query, db);
                var result = int.Parse(cmd.ExecuteScalar().ToString());
                db.Close();
                return result;
            }
            catch (MySqlException ex)
            {
                throw new Exception(SQLException.getMessageByNumberError(ex.Number));
            }
            catch (Exception ex)
            {
                throw new Exception(ErrorMessage._ERR_NO_EXECUTE_QUERY, ex);
            }
        }

        private static MySqlConnection createConnection()
        {
            try
            {
                MySqlConnection con;
                var cadenaConexion = String.Format(Constants._CONTROLRRHH_CONN_STRING, dc.ip, dc.port, dc.username, dc.password, dc.database);
                con = new MySqlConnection(cadenaConexion);
                con.Open();
                if (con.State == System.Data.ConnectionState.Open)
                {
                    return con;
                }
                else 
                {
                    throw new Exception(ErrorMessage._ERR_MYSQL_NO_OPEN);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ErrorMessage._ERR_MYSQL_NO_OPEN, ex);
            }
        }
    }
}
