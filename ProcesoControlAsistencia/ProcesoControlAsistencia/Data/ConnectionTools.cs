﻿using System;
using System.Collections.Generic;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProcesoControlAsistencia.Domain;

namespace ProcesoControlAsistencia.Data
{
    public static class ConnectionTools
    {
        public static void ChangeDatabaseExactus(
       this EXACTUSEntities source,
       string initialCatalog = "",
       string dataSource = "",
       string userId = "",
       string password = "",
       string configConnectionStringName = "")
        {
            try
            {
                var configNameEf = string.IsNullOrEmpty(configConnectionStringName)
                    ? source.GetType().Name
                    : configConnectionStringName;

                var entityCnxStringBuilder = new EntityConnectionStringBuilder
                    (System.Configuration.ConfigurationManager
                        .ConnectionStrings[configNameEf].ConnectionString);

                var sqlCnxStringBuilder = new SqlConnectionStringBuilder
                    (entityCnxStringBuilder.ProviderConnectionString);

                if (!string.IsNullOrEmpty(initialCatalog))
                    sqlCnxStringBuilder.InitialCatalog = initialCatalog;
                if (!string.IsNullOrEmpty(dataSource))
                    sqlCnxStringBuilder.DataSource = dataSource;
                if (!string.IsNullOrEmpty(userId))
                    sqlCnxStringBuilder.UserID = userId;
                if (!string.IsNullOrEmpty(password))
                    sqlCnxStringBuilder.Password = password;

                source.Database.Connection.ConnectionString
                    = sqlCnxStringBuilder.ConnectionString;
            }
            catch (Exception ex)
            {
                throw new Exception(ErrorMessage._ERR_CHANGECOFIGCONN, ex);
            }
        }


      public static void ChangeDatabaseSoftlandca(
       this SOFTLANDCAEntities source,
       string initialCatalog = "",
       string dataSource = "",
       string userId = "",
       string password = "",
       string configConnectionStringName = "")
        {
            try
            {
                var configNameEf = string.IsNullOrEmpty(configConnectionStringName)
                    ? source.GetType().Name
                    : configConnectionStringName;

                var entityCnxStringBuilder = new EntityConnectionStringBuilder
                    (System.Configuration.ConfigurationManager
                        .ConnectionStrings[configNameEf].ConnectionString);

                var sqlCnxStringBuilder = new SqlConnectionStringBuilder
                    (entityCnxStringBuilder.ProviderConnectionString);

                if (!string.IsNullOrEmpty(initialCatalog))
                    sqlCnxStringBuilder.InitialCatalog = initialCatalog;
                if (!string.IsNullOrEmpty(dataSource))
                    sqlCnxStringBuilder.DataSource = dataSource;
                if (!string.IsNullOrEmpty(userId))
                    sqlCnxStringBuilder.UserID = userId;
                if (!string.IsNullOrEmpty(password))
                    sqlCnxStringBuilder.Password = password;

                source.Database.Connection.ConnectionString
                    = sqlCnxStringBuilder.ConnectionString;
            }
            catch (Exception ex)
            {
                throw new Exception(ErrorMessage._ERR_CHANGECOFIGCONN, ex);
            }
        }
    }

    public static class SQLException
    {
        public static String getMessageByNumberError(int number)
        {
            switch (number)
            {
                case 1062:
                    return ErrorMessage._ERR_DUPLICATE_KEY;
                case 1064:
                    return ErrorMessage._ERR_SINTAX;
                default:
                    return String.Format(ErrorMessage._ERR_DB_GENERAL, number.ToString());
            }
        }
    }
}
