﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcesoControlAsistencia.Data
{

    public class CentroCosto
    {
        public String nombre { get; set; }
        public String codigo { get; set; }
    }

    public class Empleado
    {
        public String empleado { get; set; }
        public String nombre { get; set; }
        public String activo { get; set; }
        public String centro_costo { get; set; }
        public String centro_costo_nombre { get; set; }
    }

    public class Marca
    {
        public String empleado;
        public long registro;
        public DateTime fecha;
    }

    public class Usuario
    {
        public String nombre { get; set; }
        public String usuario { get; set; }
        public String contrasenia { get; set; }
        public String estado { get; set; }
        public List<Permiso> permisos { get; set; }
    }

    public class AccionPersonal
    {
        public int numero;
        public String empleado;
        public String tipo_accion;
        public String tipo_ausencia;
        public DateTime fecha_rige;
        public DateTime? fecha_vence;
        public String usuario_migra;
        public String usuario_aprueba;
    }

    public class Falta
    {
        public String empleado { get; set; }
        public String nombre_empleado { get; set; }
        public String centro_costo { get; set; }
        public String fecha_entrada { get; set; }
        public String fecha_registro { get; set; }
        public String descuento { get; set; }
    }

    public class Parametro
    {
        public Parametro(){}
        public Parametro(String key)
        {
            this.clave = key;
        }
        public String clave { get; set; }
        public String valor { get; set; }
    }

    public class Permiso
    {
        public String cod { get; set; }
        public String descripcion { get; set; }
    }
}
