﻿using ProcesoControlAsistencia.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcesoControlAsistencia.Data
{
    public static class ExactusOperations
    {
        public static DataConnection dc;

        public static List<Empleado> getEmpleadosSoftActivos(List<CentroCosto> centros)
        {
            try
            {
                var centrosS = new List<String>();
                foreach (var centro in centros)
                {
                    centrosS.Add(centro.codigo);
                }
                using (var db = new EXACTUSEntities())
                {
                    ChangeDatabase(db);
                    var marcas = new List<Empleado>();
                    marcas = (from u in db.EMPLEADO
                              where u.ACTIVO == "S" 
                              && (u.RUBRO1 != "1" || u.RUBRO1 == null)
                              where centrosS.Contains(u.CENTRO_COSTO)
                              select new Empleado
                              {
                                  empleado = u.EMPLEADO1,
                                  nombre = u.NOMBRE,
                                  activo = u.ACTIVO,
                                  centro_costo = u.CENTRO_COSTO,
                                  centro_costo_nombre = u.CENTRO_COSTO1.DESCRIPCION
                              }
                                  ).ToList();
                    return marcas;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<CentroCosto> getCentrosCosto()
        {
            try
            {
                using (var db = new EXACTUSEntities())
                {
                    ChangeDatabase(db);
                    var result = new List<CentroCosto>();
                    result = (from cc in db.CENTRO_COSTO
                              select new CentroCosto { nombre = cc.DESCRIPCION, codigo = cc.CENTRO_COSTO1 }
                              ).ToList<CentroCosto>();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<AccionPersonal> getAccionesEmpleado(Empleado empleado, DateTime fecha)
        {
            try
            {
                using (var db = new EXACTUSEntities())
                {
                    ChangeDatabase(db);
                    var result = new List<AccionPersonal>();
                    result = (from ap in db.EMPLEADO_ACC_PER
                              join ta in db.TIPO_ACCION on ap.TIPO_ACCION equals ta.TIPO_ACCION1
                              where ( EntityFunctions.TruncateTime(ap.FECHA_RIGE) <= EntityFunctions.TruncateTime(fecha)
                              && EntityFunctions.TruncateTime(fecha) <= (ap.FECHA_VENCE.HasValue 
                              ? EntityFunctions.TruncateTime(ap.FECHA_VENCE.Value)
                              : EntityFunctions.TruncateTime(ap.FECHA_RIGE)))
                              && ta.TEXTO_PREDEF.Contains("1")
                              && ta.TEXTO_PREDEF.StartsWith("1")
                              && ta.TEXTO_PREDEF.EndsWith("1")
                              && ap.EMPLEADO == empleado.empleado
                              select new AccionPersonal
                              {
                                  fecha_rige = ap.FECHA_RIGE,
                                  fecha_vence = ap.FECHA_VENCE,
                                  tipo_accion = ap.TIPO_ACCION
                              }
                              ).ToList<AccionPersonal>();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String getNombreTipoAccion(String id)
        {
            try
            {
                using (var db = new EXACTUSEntities())
                {
                    ChangeDatabase(db);
                    var result = "";
                    result = (from ta in db.TIPO_ACCION
                              where ta.TIPO_ACCION1 == id
                              select ta.DESCRIPCION
                              ).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String getNombreTipoAusencia(String id)
        {
            try
            {
                using (var db = new EXACTUSEntities())
                {
                    ChangeDatabase(db);
                    var result = "";
                    result = (from ta in db.TIPO_AUSENCIA
                              where ta.TIPO_AUSENCIA1 == id
                              select ta.DESCRIPCION
                              ).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void addAccionPersonal(AccionPersonal accion)
        {
            try
            {
                using (var db = new EXACTUSEntities())
                {
                    ChangeDatabase(db);
                    var acc = new EMPLEADO_ACC_PER();
                    var today = DateTime.Now;
                    acc.NUMERO_ACCION = accion.numero;
                    acc.EMPLEADO = accion.empleado;
                    acc.FECHA = today;
                    acc.FECHA_HORA = today;//-----------
                    acc.TIPO_ACCION = accion.tipo_accion;
                    acc.TIPO_AUSENCIA = accion.tipo_ausencia;
                    acc.FECHA_RIGE = accion.fecha_rige;
                    acc.FECHA_VENCE = accion.fecha_vence;
                    acc.USUARIO = accion.usuario_migra;
                    acc.APROBADA_POR = accion.usuario_aprueba;
                    acc.FECHA_APROBACION = today;
                    acc.ESTADO_ACCION = "S";
                    acc.DIAS_ACCION = 1;
                    acc.SALDO = 1;
                    acc.RowPointer = Guid.NewGuid();
                    db.EMPLEADO_ACC_PER.Add(acc);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool existsAccionPersonal(AccionPersonal accion)
        {
            try
            {
                using (var db = new EXACTUSEntities())
                {
                    ChangeDatabase(db);
                    var emp = (from a in db.EMPLEADO_ACC_PER
                              where a.EMPLEADO == accion.empleado
                              && a.TIPO_ACCION == accion.tipo_accion
                              && a.TIPO_AUSENCIA == accion.tipo_ausencia
                              && a.FECHA_RIGE == accion.fecha_rige
                              && a.FECHA_VENCE == accion.fecha_vence
                              select new { empleado = a.EMPLEADO }
                              ).FirstOrDefault();
                    return emp != null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int getNumberAccion()
        {
            try
            {
                using (var db = new EXACTUSEntities())
                {
                    ChangeDatabase(db);
                    var global = (from g in db.GLOBALES_RH select new { num = g.ULTIMA_ACCION }).FirstOrDefault();
                    return global == null? 0 : global.num;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void updateNumberAction(int number)
        {
            try
            {
                using (var db = new EXACTUSEntities())
                {
                    ChangeDatabase(db);
                    db.Database.ExecuteSqlCommand(String.Format(SQLQuery._Q_UPDATE_GLOBALS_RH, number + 1, number));
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void ChangeDatabase(EXACTUSEntities db)
        {
            db.ChangeDatabaseExactus(
                dataSource: String.Format("{0},{1}", dc.ip, dc.port),
                userId: dc.username,
                password: dc.password,
                initialCatalog: dc.database,
                configConnectionStringName: Constants._EXACTUS_ENTITY_NAME
                );
        }

    }
}
