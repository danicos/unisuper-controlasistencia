﻿using ProcesoControlAsistencia.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcesoControlAsistencia.Data
{
    public static class SoftlandcaOperations
    {
        public static DataConnection dc;

        public static List<Marca> getMarcasByDate(DateTime date)
        {
            try
            {
                using (var db = new SOFTLANDCAEntities())
                {
                    ChangeDatabase(db);
                    var marcas = new List<Marca>();
                    marcas = (from m in db.marcas_proceso
                                  where m.fecha_entra == date
                                  select new Marca
                                  {
                                      empleado = m.idnumero,
                                      registro = m.idregistro,
                                      fecha = m.fecha_entra
                                  }
                                  ).ToList();
                    return marcas;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("", ex);
            }
        }

        private static void ChangeDatabase(SOFTLANDCAEntities db)
        {
            db.ChangeDatabaseSoftlandca(
                dataSource: String.Format("{0},{1}", dc.ip, dc.port),
                userId: dc.username,
                password: dc.password,
                initialCatalog: dc.database,
                configConnectionStringName: Constants._SOFTLANDCA_ENTITY_NAME
                );
        }
    }
}
