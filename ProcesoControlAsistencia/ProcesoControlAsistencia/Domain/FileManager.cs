﻿using ProcesoControlAsistencia.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ProcesoControlAsistencia.Domain
{

    public class FileManager
    {
        private DataConnection readConnectionFile(String fileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(Path.GetFullPath(fileName));
                var child = doc.DocumentElement.ChildNodes[0];
                String ip = child.ChildNodes[0].InnerText;
                String database = child.ChildNodes[1].InnerText;
                String username = child.ChildNodes[2].InnerText;
                String password = child.ChildNodes[3].InnerText;
                String port = child.ChildNodes[4].InnerText;
                return new DataConnection(ip, port, database, username, UniCrypto.decryptInnova(password));
            }
            catch (Exception ex)
            {
                throw new Exception("Error al leer archivo XML", ex);
            }
        }

        public void readExactusConnectionConfig()
        {
            try
            {
                FileManager fm = new FileManager();
                ExactusOperations.dc = fm.readConnectionFile(Constants._EXACTUS_FILE_NAME);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format(ErrorMessage._ERR_READCONFIG, Constants._EXACTUS_FILE_NAME), ex);
            }
        }

        public void readSoftlandcaConnectionConfig()
        {
            try
            {
                FileManager fm = new FileManager();
                SoftlandcaOperations.dc = fm.readConnectionFile(Constants._SOFTLANDCA_FILE_NAME);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format(ErrorMessage._ERR_READCONFIG, Constants._SOFTLANDCA_FILE_NAME), ex);
            }
        }

        public void readComtrolRRHHConnectionConfig()
        {
            try
            {
                FileManager fm = new FileManager();
                ControlrrhhOperations.dc = fm.readConnectionFile(Constants._CONTROLRRHH_FILE_NAME);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format(ErrorMessage._ERR_READCONFIG,Constants._CONTROLRRHH_FILE_NAME), ex);
            }
        }
    }

    public class DataConnection
    {
        public String ip;
        public String port;
        public String database;
        public String username;
        public String password;

        public DataConnection(String ip = "", String port = "", String database = "", String username = "", String password = "")
        {
            this.ip = ip;
            this.port = port;
            this.database = database;
            this.username = username;
            this.password = password;
        }
    }
}
