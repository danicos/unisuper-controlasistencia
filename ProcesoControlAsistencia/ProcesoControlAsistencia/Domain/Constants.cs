﻿using ProcesoControlAsistencia.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcesoControlAsistencia.Domain
{
    public static class Constants
    {
        public const String _EXACTUS_FILE_NAME = "DBExactus.xml";
        public const String _SOFTLANDCA_FILE_NAME = "DBSoftlandca.xml";
        public const String _CONTROLRRHH_FILE_NAME = "DBControlrrhh.xml";
        public const String _EXACTUS_ENTITY_NAME = "EXACTUSEntities";
        public const String _SOFTLANDCA_ENTITY_NAME = "SOFTLANDCAEntities";
        public const String _CONTROLRRHH_CONN_STRING = "server={0};port={1};user id={2}; password={3}; database={4}";
        public static readonly Object[] _USER_STATES = { new Parametro { clave = "Activo", valor = "A" }, new Parametro { clave = "Inactivo", valor = "I" } };
        public static readonly String[] _DB_PARAMETROS = { "TIPAC", "TIPAU", "USMIG", "USAPR", "NOFAL" };
        public const String _K_CR_UNI = "!7aR^4#qOm-";
        public const String _IN_CR_UNI = "Q|&#8@$+(";
        public const int _I_CR_UNI = 1000;
        public static readonly String[] _DB_PER = { "EJPRO", "MANCC", "MANVA", "MANUS" };
    }

    public static class ErrorMessage
    {
        public const String _ERR_READCONFIG = "Error al leer archivo de configuración de base de datos. <{0}>";
        public const String _ERR_CHANGECOFIGCONN = "Error al actualizar datos de conección.";
        public const String _ERR_MYSQL_NO_OPEN = "Conección a base de datos no establecida.";
        public const String _ERR_NO_EXECUTE_QUERY = "Error al ejecutar instrucción a la base de datos.";
        public const String _ERR_DB_GENERAL = "Error al procesar instrucción a la base de datos. Codigo: {0}";
        public const String _ERR_DUPLICATE_KEY = "El registro ya fue agregado anteriormente.";
        public const String _ERR_SINTAX = "Instrucción inválida.";
        public const String _ERR_CRYPTO = "Error en manejo de claves.";
        public const String _LOG_NOCENTROCOSTO = "El proceso ya ha sido ejecutado anteriormente para el centro de costo <{0}>.";
        public const String _LOG_PROCESOEXITO = "Proceso ejecutado con éxito para el centro de costo <{0}>.";
        public const String _LOG_NOEJECUTADO = "El proceso no pudo ser ejecutado en ningun centro de costo.";
    }

    public static class SQLQuery
    {
        public const String _Q_SELECT_CENTROCOSTO = "SELECT centrocosto, nombre FROM centrocosto ORDER BY rowindex ASC LIMIT {0},{1};";
        public const String _Q_SELECT_ALL_CENTROCOSTO = "SELECT centrocosto, nombre FROM centrocosto;";
        public const String _Q_COUNT_CENTROCOSTO = "SELECT COUNT(*) FROM centrocosto;";
        public const String _Q_INSERT_CENTROCOSTO = "INSERT INTO centrocosto(centrocosto, nombre) VALUES ('{0}','{1}');";
        public const String _Q_DELETE_CENTROCOSTO = "DELETE FROM centrocosto WHERE centrocosto = '{0}'";
        public const String _Q_DELETE_ALL_EMPLEADORELOJ = "DELETE FROM empleadosreloj;";
        public const String _Q_INSERT_EMPLEADORELOJ = "INSERT INTO empleadosreloj (reloj_idregistro, reloj_codempleado, reloj_fechaentrada) VALUES ('{0}','{1}','{2}-{3}-{4}');";
        public const String _Q_DELETE_ALL_EMPLEADOSOFT = "DELETE FROM empleadossoftland;";
        public const String _Q_INSERT_EMPLEADOSOFT = "INSERT INTO empleadossoftland (soft_codempleado, soft_nombre, soft_activo, soft_centrocosto, soft_nombrecentrocosto) VALUES ('{0}','{1}','{2}','{3}','{4}');";
        public const String _Q_SELECT_EMPLEADOS_NO_MARCA = "SELECT es.`soft_codempleado`, es.`soft_nombre`, es.`soft_activo`, es.`soft_centrocosto`, es.`soft_nombrecentrocosto` FROM empleadossoftland es LEFT JOIN empleadosreloj er ON es.`soft_codempleado` = er.`reloj_codempleado` WHERE er.`reloj_codempleado` IS NULL";
        public const String _Q_SELECT_LOG_NO_DESCUENTO = "SELECT le.`log_codempleado` AS 'codempleado', le.`log_fechaentrada` AS 'fechaentrada' FROM logcontrolentradas le WHERE le.`log_codempleado` = '{0}' AND (le.`log_fechaentrada` BETWEEN '{1}-{2}-{3}' AND '{4}-{5}-{6}') AND le.`log_descuento` = 'N';";
        public const String _Q_UPDATE_PARAMETRO = "UPDATE parametroconfig SET `valor` = '{0}' WHERE `clave` = '{1}';";
        public const String _Q_SELECT_PARAMETRO = "SELECT `clave`, `valor` FROM `parametroconfig` WHERE clave = '{0}';";
        public const String _Q_INSERT_USUARIO = "INSERT INTO `usuario` (`usuario`, `nombre`, `contrasenia`, `estado`) VALUES ('{0}', '{1}', '{2}', '{3}');";
        public const String _Q_SELECT_USUARIO = "SELECT usuario, nombre, estado FROM usuario ORDER BY rowindex ASC LIMIT {0},{1};";
        public const String _Q_INSERT_LOGENTRADA = "INSERT INTO `logcontrolentradas` (`log_codempleado`, `log_nombreempleado`, `log_centrocosto`, `log_fechaentrada`, `log_fecharegistro`, `log_descuento`) VALUES ('{0}', '{1}', '{2}', '{3}-{4}-{5}', '{6}-{7}-{8}', '{9}');";
        public const String _Q_INSERT_LOGPROCESO = "INSERT INTO `logproceso` (`logp_fechaentrada`, `logp_centrocosto`, `logp_fecharegistro`) VALUES ('{0}-{1}-{2}', '{3}', '{4}-{5}-{6}');";
        public const String _Q_SELECT_LOGPROCESO = "SELECT `logp_fechaentrada`, `logp_fecharegistro` FROM `logproceso` WHERE `logp_fechaentrada` = '{0}-{1}-{2}' AND `logp_centrocosto` = '{3}';";
        public const String _Q_COUNT_USUARIOS = "SELECT COUNT(*) FROM usuario;";
        public const String _Q_DELETE_USUARIO = "DELETE FROM `usuario` WHERE `usuario` = '{0}';";
        public const String _Q_SELECT_USUARIOBYID = "SELECT `usuario`, `nombre`, `estado` FROM `usuario` WHERE `usuario` = '{0}';";
        public const String _Q_SELECT_PERMISOS = "SELECT `cod`, `descripcion` FROM `permiso`;";
        public const String _Q_SELECT_PERMISOSUS = "SELECT `permiso` AS cod FROM `usuario_permiso` WHERE `usuario` = '{0}';";
        public const String _Q_INSERT_PERMISO = "INSERT INTO `usuario_permiso` (`usuario`, `permiso`) VALUES ('{0}', '{1}');";
        public const String _Q_REMOVE_PERMISOS = "DELETE FROM `usuario_permiso` WHERE `usuario` = '{0}';";
        public const String _Q_UPDATE_USUARIO = "UPDATE `usuario` SET `nombre` = '{0}', `estado` = '{1}' WHERE `usuario` = '{2}';";
        public const String _Q_UPDATE_USUARIOPASS = "UPDATE `usuario` SET `nombre` = '{0}', `contrasenia` = '{1}', `estado` = '{2}' WHERE `usuario` = '{3}';";
        public const String _Q_SELECT_USUARIOPASS = "SELECT `usuario`, `nombre`, `estado` FROM `usuario` WHERE `usuario` = '{0}' and `contrasenia` = '{1}';";
        public const String _Q_UPDATE_GLOBALS_RH = "UPDATE UNISUP.GLOBALES_RH SET ULTIMA_ACCION = '{0}' WHERE ULTIMA_ACCION = '{1}';";
    }

}
