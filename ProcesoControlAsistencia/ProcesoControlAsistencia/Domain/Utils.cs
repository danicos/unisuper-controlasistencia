﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ProcesoControlAsistencia.Domain
{
    public static class Utils
    {
        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
    }

    public static class UniCrypto
    {
        public static String encryptKey(String rawdata)
        {
            String _claveSalt = Constants._K_CR_UNI + rawdata + Constants._K_CR_UNI;
            var _objSha256 = new SHA1Managed();
            Byte[] _objTemporal;
            try
            {
                _objTemporal = System.Text.ASCIIEncoding.UTF8.GetBytes(_claveSalt);
                for (int i = 0; i < Constants._I_CR_UNI; i++)
                {
                    _objTemporal = _objSha256.ComputeHash(_objTemporal);
                }
                var str = Convert.ToBase64String(_objTemporal);
                str = str.Replace("'", "+");
                str = str.Replace("\"", "+");
                return str;
            }
            catch (Exception ex)
            {
                throw new Exception(ErrorMessage._ERR_CRYPTO, ex);
            }
        }

        public static String encryptInnova(String rawdata)
        {
            try
            {
                var des = new TripleDESCryptoServiceProvider();
                des.IV = new Byte[8];
                var pdb = new PasswordDeriveBytes(Constants._IN_CR_UNI, new Byte[] { });
                des.Key = pdb.CryptDeriveKey("RC2", "MD5", 128, new Byte[8]);
                var ms = new MemoryStream((rawdata.Length * 2) - 1);
                var encStream = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write);
                var plainBytes = ASCIIEncoding.Default.GetBytes(rawdata);
                encStream.Write(plainBytes, 0, plainBytes.Length);
                encStream.FlushFinalBlock();
                var encryptedBytes = new Byte[ms.Length];
                ms.Position = 0;
                ms.Read(encryptedBytes, 0, (int)ms.Length);
                encStream.Close();
                return Convert.ToBase64String(encryptedBytes);
            }
            catch (Exception ex)
            {
                throw new Exception(ErrorMessage._ERR_CRYPTO, ex);
            }
        }

        public static String decryptInnova(String rawdata)
        {
            try
            {
                var des = new TripleDESCryptoServiceProvider();
                des.IV = new Byte[8];
                var pdb = new PasswordDeriveBytes(Constants._IN_CR_UNI, new Byte[] { });
                des.Key = pdb.CryptDeriveKey("RC2", "MD5", 128, new Byte[8]);
                var enctyptedBytes = Convert.FromBase64String(rawdata);
                var ms = new MemoryStream(rawdata.Length);
                var decStream = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write);
                decStream.Write(enctyptedBytes, 0, enctyptedBytes.Length);
                decStream.FlushFinalBlock();
                var plainBytes = new Byte[ms.Length];
                ms.Position = 0;
                ms.Read(plainBytes, 0, (int)ms.Length);
                decStream.Close();
                return ASCIIEncoding.UTF8.GetString(plainBytes);
            }
            catch (Exception ex)
            {
                throw new Exception(ErrorMessage._ERR_CRYPTO, ex);
            }
        }
    }
}
