﻿using ProcesoControlAsistencia.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcesoControlAsistencia.Domain
{
    public static class Proceso
    {
        public static void ejecutarProceso(DateTime date, CentroCosto selectedCentro = null, Action<double> Estado = null, Action<String> Log = null, Action<Empleado, DateTime, Boolean> Reporte = null)
        {
            try
            {
                List<CentroCosto> centrosdb;
                //Se verifica si existe algun centro de costo especifico para la ejecucion del proceso
                if (selectedCentro == null)
                {
                    //Se obtienen todos los centros de costos permitidos
                    centrosdb = ControlrrhhOperations.getCentrosCosto();
                }
                else
                {
                    //Se agrega el centro de costo seleccionado por el usuario
                    centrosdb = new List<CentroCosto>();
                    centrosdb.Add(selectedCentro);
                }
                if (Estado != null) Estado(5);

                //Se inicializa la lista de centros que sera utilizada para el proceso
                var centrosEjecutar = new List<CentroCosto>();

                foreach (var centro in centrosdb)
                {
                    //Comprobar si ya se ejecuto el proceso anteriormente en ese centro de costo
                    var logproc = ControlrrhhOperations.existsLogProceso(date, centro);
                    if (logproc)
                    {
                        if (Log != null) Log(String.Format(ErrorMessage._LOG_NOCENTROCOSTO, centro.nombre));
                        continue;
                    }
                    centrosEjecutar.Add(centro);
                }
                if (Estado != null) Estado(10);

                //Verificar si existe algun centro de costo para ejecutar el proceso
                if (centrosEjecutar.Count == 0)
                {
                    //Se agrega un log con lo sucedido y se termina la ejecucion
                    if (Log != null) Log(ErrorMessage._LOG_NOEJECUTADO);
                    if (Estado != null) Estado(100);
                    return;
                }

                //Se limpia la tabla empleadossoftland de mysql
                ControlrrhhOperations.removeAllEmpleadoSoft();
                //Se extraen los usuarios activos de EXACTUS que coincidan con los centros de costo ingresados
                var empleadosSoft = ExactusOperations.getEmpleadosSoftActivos(centrosEjecutar);
                //Llenado de la tabla empleadossoft con la extraccion de empleados 
                ControlrrhhOperations.addEmpleadoSoft(empleadosSoft, Estado);
                if (Estado != null) Estado(35);

                //Se limpia la tabla empleadosreloj de mysql
                ControlrrhhOperations.removeAllEmpleadoReloj();
                //Extraccion de marcas de reloj
                var marcas = SoftlandcaOperations.getMarcasByDate(date);
                //Llenado de la tabla empleadosreloj con la extraccion de las marcas 
                ControlrrhhOperations.addEmpleadoReloj(marcas, Estado);
                if (Estado != null) Estado(70);

                //Obtener empleados que no hayan tenido marcaje en el día
                var empleadosNoMarca = ControlrrhhOperations.getEmpleadosNoMarcaje();
                double aum = 20 / empleadosNoMarca.Count();
                double val = 70;
                //Validar si se le aplica accion de descuento al empleado
                foreach (var empleado in empleadosNoMarca)
                {
                    val += aum;
                    if (Estado != null) Estado(val);
                    //Extraccion de acciones permitidas para faltas
                    var acciones = ExactusOperations.getAccionesEmpleado(empleado, date);
                    //Se verifica si existen acciones permitidas para la falta del dia
                    if (acciones.Count() == 0)
                    {
                        //Extraccion de faltas en la semana
                        var faltas = ControlrrhhOperations.getFaltasByRangeDate(empleado, startWeek(date), endWeek(date));
                        //Extraccion de parametro de maximo de faltas
                        var maxfaltas = ControlrrhhOperations.getParametro(Constants._DB_PARAMETROS[4]);
                        int max = maxfaltas != null ? int.Parse(maxfaltas.valor) : 1;
                        //Se verifica si ya existe al menos una falta en la semana
                        if (faltas.Count() >= max)
                        {
                            //Si ya existe al menos una falta en la semana se procede a ingresar una accion de descuento

                            //Obtencion de parametros configurados
                            var tipoaccion = ControlrrhhOperations.getParametro(Constants._DB_PARAMETROS[0]);
                            var tipoausencia = ControlrrhhOperations.getParametro(Constants._DB_PARAMETROS[1]);
                            var usmigracion = ControlrrhhOperations.getParametro(Constants._DB_PARAMETROS[2]);
                            var usaprobar = ControlrrhhOperations.getParametro(Constants._DB_PARAMETROS[3]);
                            //Creacion de la accion de personal
                            var accionPersonal = new AccionPersonal();
                            accionPersonal.empleado = empleado.empleado;
                            accionPersonal.fecha_rige = date;
                            accionPersonal.fecha_vence = date;
                            accionPersonal.tipo_accion = tipoaccion != null ? tipoaccion.valor : "";
                            accionPersonal.tipo_ausencia = tipoausencia != null ? tipoausencia.valor : "";
                            accionPersonal.usuario_migra = usmigracion != null ? usmigracion.valor : "";
                            accionPersonal.usuario_aprueba = usaprobar != null ? usaprobar.valor : "";
                            //Verificacion de si ya existe la accion de personal
                            var existeAccion = ExactusOperations.existsAccionPersonal(accionPersonal);
                            if (!existeAccion)
                            {
                                //Obtencion de correlativo de accion de personal
                                accionPersonal.numero = ExactusOperations.getNumberAccion();
                                //Agregacion de la nueva accion de personal
                                ExactusOperations.addAccionPersonal(accionPersonal);
                                //Actualizacion de correlativo de accion de personal
                                ExactusOperations.updateNumberAction(accionPersonal.numero);
                                //Agregacion de la accion en el log con descuento
                                ControlrrhhOperations.addLogEntrada(empleado, date, true);
                                if (Reporte != null) Reporte(empleado, date, true);
                            }
                        }
                        else
                        {
                            //Agregacion de la accion en el log sin descuento
                            ControlrrhhOperations.addLogEntrada(empleado, date, false);
                            if (Reporte != null) Reporte(empleado, date, false);
                        }
                    }
                }

                if (Estado != null) Estado(90);

                foreach (var centro in centrosEjecutar)
                {
                    //Agregar log de proceso
                    ControlrrhhOperations.addLogProceso(date, centro);
                    if (Log != null) Log(String.Format(ErrorMessage._LOG_PROCESOEXITO, centro.nombre));
                }

                if (Estado != null) Estado(100);
            }
            catch (Exception ex)
            {
                if (Estado != null) Estado(100);
                throw ex;
            }
        }

        private static DateTime startWeek(DateTime date)
        {
            return date.AddDays((((int)date.DayOfWeek) - 1) * -1);
        }

        private static DateTime endWeek(DateTime date)
        {
            return date.AddDays(7 - ((int)date.DayOfWeek));
        }
    }
}
